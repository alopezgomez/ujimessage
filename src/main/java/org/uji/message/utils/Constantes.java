package org.uji.message.utils;

import java.util.Arrays;
import java.util.List;

public final class Constantes {

	private Constantes() {

	}

	public static final String EMPTY = "";
	public static final String RIGHT_CURLY_BRACKET = "}";
	public static final String LEFT_CURLY_BRACKET = "{";
	public static final String COMMA = ",";
	public static final String DOT = ".";
	public static final String LINE_FEED = "\n";

	public static final String DEFAULT_VALUE_PROJECTION = "1";
	public static final String OPERATOR_PUSH = "$push";
	public static final String OPERATOR_UNSET = "$unset";
	public static final String OPERATOR_PULL = "$pull";

	public static final String TOKEN = "token";
	public static final String CREATED_DATA = "createdData";
	public static final String USER_ALLOW = "userAllow";
	public static final Long TOKEN_EXPIRATION_DEFAULT = 30l;
	public static final String MSG_ERR_NEW_TOKEN = "Debe solicitar un token nuevo";
	public static final String MSG_ERR_CHAT_NOT_EXISTS_USERS_IN_DOMAIN = "No existen los usuarios para el dominio";

	public static final List<String> PATHS_WITHOT_TOKEN = Arrays
			.asList(new String[] { "/auth/token", "/register/domain" });
	public static final int FIRS_RESULT = 0;

	public static final String QUERY_ID_GENERADO = "queryIdGenerado";
	public static final String QUERY_USER = "queryUser";
	public static final String QUERY_ADMIN = "queryAdmin";
	public static final String QUERY_FOR_MESSAGE = "queryForMessage";
	public static final String SEQUENCES = "sequence";
	public static final String QUERY_MESSAGES = "queryMessagesUser";
	public static final String QUERY_ALL_MESSAGE = "queryAllMessage";
	public static final String QUERY_ELEM_MATCH_USERNAME = "queryElemMathListUserName";
	public static final String QUERY_ELEM_MATCH = "queryElemMathListUser";
	public static final String QUERY_ADMIN_USER = "queryUserAdminName";

}
