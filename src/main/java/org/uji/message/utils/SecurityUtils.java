package org.uji.message.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.uji.message.security.Token;
import org.uji.message.security.TypeSecurity;

/**
 * Clase que contiene las utilidades en las que se apoya la securización de la
 * aplicación
 * 
 * @author angel
 * */
public final class SecurityUtils {

	private SecurityUtils() {

	}

	private static MessageDigest messageDigest;

	public static Token getSecurityHashed(TypeSecurity typeSecurity,
			String[] parms) throws NoSuchAlgorithmException {
		messageDigest = MessageDigest.getInstance("MD5");
		String secretKey = "";
		switch (typeSecurity) {
		case PASSWORD:
			secretKey = "daer?da77871jhjkhdas8#";
			break;

		case SESSION_TOKEN:
			secretKey = "P|OP¿Ohkj1h82+-@d!";
			break;
		}

		return typeSecurity.generateHashedSecurity(parms, secretKey,
				messageDigest);

	}

}
