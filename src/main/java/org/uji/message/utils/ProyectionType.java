package org.uji.message.utils;

/**
 * Enumeracion que contiene los tipos de proyecciones utilizadas en la anotacion
 * {@link org.uji.message.annotations.dao.ProjectionProjection}
 * 
 * @author angel
 * */
public enum ProyectionType {

	TYPE, LIST, SIMPLE

}
