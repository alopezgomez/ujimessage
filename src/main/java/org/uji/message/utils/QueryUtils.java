package org.uji.message.utils;

import org.apache.commons.lang3.StringUtils;
import org.uji.message.annotations.dao.JSONQuery;
import org.uji.message.annotations.dao.JSONQuerys;
import org.uji.message.dto.MongoDTOObject;

/***
 * Clase que resuelve las querys indicadas en las anotaciones {@link JSONQuery}
 * y {@link JSONQuerys}
 * 
 * @author angel
 **/
public final class QueryUtils {

	private QueryUtils() {

	}

	public static String getJSONQuery(String nameQuery,
			Class<? extends MongoDTOObject> mongoDTOClass, Object[] parms) {

		String query = "";

		if (mongoDTOClass.isAnnotationPresent(JSONQuerys.class)) {
			JSONQuerys annotation = mongoDTOClass
					.getAnnotation(JSONQuerys.class);
			JSONQuery[] jsonQueries = annotation.value();
			for (int i = 0; i < jsonQueries.length
					&& StringUtils.isBlank(query); i++) {
				JSONQuery jsonQuery = jsonQueries[i];
				query = findQuery(nameQuery, jsonQuery);

			}
		}

		if (mongoDTOClass.isAnnotationPresent(JSONQuery.class)) {
			JSONQuery annotation = mongoDTOClass.getAnnotation(JSONQuery.class);
			query = findQuery(nameQuery, annotation);

		}

		if (isQueryParameterized(query, parms)) {
			query = resolveParameterizer(query, parms);
		}
		return query;
	}

	private static String resolveParameterizer(String query, Object[] parms) {

		for (int j = 0; j < parms.length; j++) {
			Object parm = parms[j];
			StringBuffer replacer = new StringBuffer();
			replacer.append(parm);
			if (parm instanceof String) {
				replacer = new StringBuffer();
				replacer.append("'").append(parm).append("'");
			}
			query = new String(query.replaceFirst("\\?", replacer.toString()));
		}

		return query;
	}

	private static String findQuery(String nameQuery, JSONQuery annotation) {
		if (nameQuery.equals(annotation.nameQuery())) {
			return annotation.query();
		}
		return null;
	}

	private static boolean isQueryParameterized(String query, Object[] parms) {
		return StringUtils.isNotBlank(query) && parms != null;
	}

}
