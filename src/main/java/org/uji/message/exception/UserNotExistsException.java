package org.uji.message.exception;

public class UserNotExistsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3365157975388937316L;

	public UserNotExistsException() {
		super();
	}

	public UserNotExistsException(String msg) {
		super(msg);
	}

	public UserNotExistsException(Exception e) {
		super(e);
	}

	@Override
	public String getMessage() {
		return "El usuario no existe con esas credenciales";
	}

}
