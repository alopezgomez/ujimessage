package org.uji.message.exception;

public class ChatNotExistException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5420301875766092094L;

	public ChatNotExistException() {
		super();
	}

	public ChatNotExistException(String message) {
		super(message);
	}

	public ChatNotExistException(Throwable cause) {
		super(cause);

	}

	@Override
	public String getMessage() {
		return "No existe chat para el usuario";
	}

}
