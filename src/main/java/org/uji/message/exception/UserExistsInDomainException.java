package org.uji.message.exception;

public class UserExistsInDomainException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4254535648674455992L;

	public UserExistsInDomainException() {
		super();
	}

	public UserExistsInDomainException(Exception e) {
		super(e);
	}

	public UserExistsInDomainException(String msg) {
		super(msg);
	}

	@Override
	public String getMessage() {

		return "El nombre de usuario está registrado ya para el dominio";
	}

}
