package org.uji.message.exception;

public class DomainAlreadyRegisteredException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2157530241763104090L;

	public DomainAlreadyRegisteredException() {
		super();
	}

	public DomainAlreadyRegisteredException(Exception e) {
		super(e);
	}

	public DomainAlreadyRegisteredException(String msg) {
		super(msg);
	}

	@Override
	public String getMessage() {
		return "El dominio ya esta registrado";
	}

}
