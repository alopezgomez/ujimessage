package org.uji.message.exception;

public class MessageNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -91247653945427206L;

	public MessageNotFoundException() {
		super();
	}

	public MessageNotFoundException(String message) {
		super(message);

	}

	public MessageNotFoundException(Throwable cause) {
		super(cause);
	}

	@Override
	public String getMessage() {
		return "No se ha encontrado el mensaje solicitado";
	}

}
