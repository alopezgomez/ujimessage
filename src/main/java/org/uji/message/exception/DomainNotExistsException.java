package org.uji.message.exception;

public class DomainNotExistsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7442873077361216933L;

	public DomainNotExistsException() {
		super();
	}

	public DomainNotExistsException(Exception e) {
		super(e);
	}

	public DomainNotExistsException(String msg) {
		super(msg);
	}

	@Override
	public String getMessage() {

		return "El dominio no existe el dominio con esos datos de entrada";
	}

}
