package org.uji.message.exception;

public class ContacNotExistsException extends UserNotExistsException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7523878916383994746L;

	public ContacNotExistsException() {
		super();
	}

	public ContacNotExistsException(String msg) {
		super(msg);
	}

	public ContacNotExistsException(Exception e) {
		super(e);
	}

	@Override
	public String getMessage() {

		return "El contacto no existe en ese dominio";
	}

}
