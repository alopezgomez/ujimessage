package org.uji.message.exception;

public class ChatNotInitException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2111105601267056869L;

	public ChatNotInitException(Exception e) {
		super(e);
	}

	public ChatNotInitException(String message) {
		super(message);
	}

	public ChatNotInitException() {
		super();
	}

	@Override
	public String getMessage() {
		return "No se ha podido iniciar el chat";
	}

}
