package org.uji.message.services;

import java.util.List;

import org.uji.message.dto.Domain;
import org.uji.message.dto.User;
import org.uji.message.exception.ContacNotExistsException;
import org.uji.message.exception.DomainNotExistsException;

/**
 * Interfaz que describe los metodos para el control de las operaciones de
 * contactos
 * 
 * @author angel
 * */
public interface ContactService {

	List<User> getAllContacts(Domain domain) throws DomainNotExistsException;

	User getContact(Domain domainFilter) throws DomainNotExistsException,
			ContacNotExistsException;

}
