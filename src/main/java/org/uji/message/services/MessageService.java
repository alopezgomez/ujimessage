package org.uji.message.services;

import java.util.List;

import org.uji.message.dto.Domain;
import org.uji.message.exception.DomainNotExistsException;
import org.uji.message.exception.MessageNotFoundException;
import org.uji.message.exception.UserNotExistsException;

/**
 * Interfaz que describe los metodos para el control de las operaciones de
 * mensajes
 * 
 * @author angel
 * */
public interface MessageService {

	static final String ACCESS_TO_COLLECTION_LISTMESSAGES = "listUsersDomain.$.listMessages";

	List<Domain> getAllMessages(Domain domain) throws DomainNotExistsException,
			UserNotExistsException, MessageNotFoundException;

	Domain getMessageForUser(Domain domain) throws MessageNotFoundException,
			DomainNotExistsException, UserNotExistsException;

	void addMessageInDomain(Domain domain) throws DomainNotExistsException,
			UserNotExistsException;

	void removeMessage(Domain domainFilter) throws DomainNotExistsException,
			UserNotExistsException;

}
