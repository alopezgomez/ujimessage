package org.uji.message.services;

import org.uji.message.dto.Domain;
import org.uji.message.exception.DomainAlreadyRegisteredException;
import org.uji.message.exception.DomainNotExistsException;

/**
 * Interfaz que describe los metodos para el control de las operaciones de
 * registro
 * 
 * @author angel
 * */
public interface RegisterService {

	void registerDomain(Domain domain) throws DomainAlreadyRegisteredException;

	void existsDomain(Domain domain) throws DomainNotExistsException;

}
