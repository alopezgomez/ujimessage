package org.uji.message.services;

import java.util.List;

import org.uji.message.dto.Domain;
import org.uji.message.exception.DomainNotExistsException;
import org.uji.message.exception.UserExistsInDomainException;
import org.uji.message.exception.UserNotExistsException;

/**
 * Interfaz que describe los metodos para el control de las operaciones de
 * administracion
 * 
 * @author angel
 * */
public interface AdminService {

	static final String ACCES_COLLECTION_LISTUSERSDOMAIN = "listUsersDomain";

	void addUsertToDomain(Domain domain) throws UserExistsInDomainException;

	void deleteDomain(Domain domain) throws DomainNotExistsException;

	void deleteUserInDomain(Domain domain) throws DomainNotExistsException,
			UserNotExistsException;

	Domain getAllUsersInDomain(Domain domain) throws DomainNotExistsException;

	List<Domain> getAllDomains();

}
