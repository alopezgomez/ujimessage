package org.uji.message.services;

import org.uji.message.dto.Chat;
import org.uji.message.exception.ChatNotExistException;
import org.uji.message.exception.ChatNotInitException;

/**
 * Interfaz que describe los metodos para el control de las operaciones de chat
 * 
 * @author angel
 * */
public interface ChatService {

	void initChat(Chat chat) throws ChatNotInitException;

	Chat getChat(Chat chat) throws ChatNotExistException;

}
