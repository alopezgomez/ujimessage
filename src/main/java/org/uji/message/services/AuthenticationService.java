package org.uji.message.services;

import org.uji.message.dto.Domain;
import org.uji.message.exception.UserNotExistsException;

/**
 * Interfaz que describe los metodos para el control de las operaciones de
 * autenticacion
 * 
 * @author angel
 * */
public interface AuthenticationService {

	Domain existsUser(Domain domain) throws UserNotExistsException;

	boolean isAdminUser(Domain domain) throws UserNotExistsException;

}
