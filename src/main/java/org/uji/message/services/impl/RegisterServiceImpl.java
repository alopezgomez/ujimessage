package org.uji.message.services.impl;

import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;

import javax.inject.Named;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.uji.message.annotations.dao.CollectionName;
import org.uji.message.dao.MongoDBDao;
import org.uji.message.dao.impl.MongoDBDaoImpl;
import org.uji.message.dto.Domain;
import org.uji.message.dto.MongoDTOObject;
import org.uji.message.dto.User;
import org.uji.message.exception.DomainAlreadyRegisteredException;
import org.uji.message.exception.DomainNotExistsException;
import org.uji.message.security.Token;
import org.uji.message.security.TypeSecurity;
import org.uji.message.services.RegisterService;
import org.uji.message.utils.SecurityUtils;

@Named("registerService")
public class RegisterServiceImpl implements RegisterService {

	private MongoDBDao<Serializable, MongoDTOObject> mongoDAO;

	public RegisterServiceImpl() {

		mongoDAO = new MongoDBDaoImpl<String, Domain>(new Domain());
	}

	@Override
	public void registerDomain(Domain domain)
			throws DomainAlreadyRegisteredException {
		try {
			Domain domainFilter = new Domain();
			domainFilter.setNomDomain(domain.getNomDomain());
			Domain domainExists = searchByQueryFilter(domainFilter);
			if (domainExists != null) {
				throw new DomainAlreadyRegisteredException();
			}
			User userAdmin = domain.getUserAdmin();
			Token tokenPassword = SecurityUtils.getSecurityHashed(
					TypeSecurity.PASSWORD,
					new String[] { userAdmin.getUserName(),
							userAdmin.getPassword() });
			String collectionName = Domain.class.getAnnotation(
					CollectionName.class).value();
			domain.setIdGenerado(mongoDAO.getSeqNumber(collectionName));
			userAdmin.setPassword(tokenPassword.getToken());
			String collectionUser = User.class.getAnnotation(
					CollectionName.class).value();
			userAdmin.setIdGenerado(mongoDAO.getSeqNumber(collectionUser));
			domain.setUserAdmin(userAdmin);
			domain.addUserToDomain(userAdmin);
			mongoDAO.insert(domain);
		} catch (IOException | NoSuchAlgorithmException e) {
			throw new DomainAlreadyRegisteredException(e);
		}
	}

	@Override
	public void existsDomain(Domain domain) throws DomainNotExistsException {
		try {

			if (searchByQueryFilter(domain) == null) {
				throw new DomainNotExistsException();
			}
		} catch (IOException e) {
			throw new DomainNotExistsException(e);
		}
	}

	private Domain searchByQueryFilter(Domain domainFilter)
			throws JsonGenerationException, JsonMappingException, IOException {
		String jsonQuery = domainFilter.toJSON();
		Domain domainExists = (Domain) mongoDAO.findOneByQuery(jsonQuery);
		return domainExists;
	}

}