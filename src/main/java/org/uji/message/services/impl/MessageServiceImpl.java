package org.uji.message.services.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Named;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.uji.message.annotations.dao.CollectionName;
import org.uji.message.dao.MongoDBDao;
import org.uji.message.dao.impl.MongoDBDaoImpl;
import org.uji.message.dto.Domain;
import org.uji.message.dto.Message;
import org.uji.message.dto.MongoDTOObject;
import org.uji.message.dto.User;
import org.uji.message.exception.DomainNotExistsException;
import org.uji.message.exception.MessageNotFoundException;
import org.uji.message.exception.UserNotExistsException;
import org.uji.message.services.MessageService;
import org.uji.message.utils.Constantes;
import org.uji.message.utils.ProjectionUtils;
import org.uji.message.utils.QueryUtils;

import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;

@Named("messageService")
@SuppressWarnings("unchecked")
public class MessageServiceImpl implements MessageService {

	private MongoDBDao<Serializable, MongoDTOObject> mongoDao;

	public MessageServiceImpl() {
		mongoDao = new MongoDBDaoImpl<String, Domain>(new Domain());
	}

	@Override
	public List<Domain> getAllMessages(Domain domain)
			throws DomainNotExistsException, UserNotExistsException {

		validateData(domain);
		try {
			BasicDBObject domainFilter = new BasicDBObject("idGenerado",
					domain.getIdGenerado());
			List<Domain> queryProjection = (List<Domain>) mongoDao
					.findJSONQueryProjection(Constantes.QUERY_ELEM_MATCH,
							new Object[] { domain.getFirstUser()
									.getIdGenerado() }, domainFilter);
			return queryProjection;
		} catch (Exception e) {
			throw new UserNotExistsException(e);

		}
	}

	public void parseMessages(User firstUser, List<Message> messageReduced,
			User userResult) {
		List<Message> messagesResult = userResult.getListMessages();
		for (Message message : messagesResult) {
			if (message.getListTo().contains(firstUser.getUserName())) {
				messageReduced.add(message);
			}
		}
	}

	@Override
	public Domain getMessageForUser(Domain domainFilter)
			throws DomainNotExistsException, UserNotExistsException,
			MessageNotFoundException {

		validateData(domainFilter);
		BasicDBObject projectionDTOObject = null;
		try {
			projectionDTOObject = ProjectionUtils
					.projection(setProjectionForFullMessage());
		} catch (Exception e) {
			throw new DomainNotExistsException(e);
		}
		BasicDBObject query = (BasicDBObject) JSON
				.parse(setDataFilterQuery(domainFilter));
		List<Domain> list = (List<Domain>) mongoDao.findUsingProjection(query,
				projectionDTOObject);

		if (list.isEmpty()) {
			throw new MessageNotFoundException();
		}
		return list.get(Constantes.FIRS_RESULT);

	}

	@Override
	public void addMessageInDomain(Domain domainData)
			throws DomainNotExistsException, UserNotExistsException {

		validateData(domainData);

		try {
			User userSender = domainData.getFirstUser();
			Message message = userSender.getFirstMessage();
			Set<String> listReceptions = setReceptions(message);
			List<User> listUser = setReceivingUser(domainData, listReceptions);
			setIdGenerado(message);
			dispatchMessageToUsers(domainData, message, listUser);
		} catch (IOException e) {
			throw new DomainNotExistsException(e);
		}

	}

	@Override
	public void removeMessage(Domain domainFilter)
			throws DomainNotExistsException, UserNotExistsException {
		validateData(domainFilter);
		User user = domainFilter.getFirstUser();

		Message message = user.getFirstMessage();
		BasicDBObject criteriaForDelete = (BasicDBObject) JSON
				.parse(setDataFilterQuery(domainFilter));
		BasicDBObject deleted = new BasicDBObject(
				Constantes.OPERATOR_PULL,
				new BasicDBObject(
						ACCESS_TO_COLLECTION_LISTMESSAGES,
						new BasicDBObject("idGenerado", message.getIdGenerado())));
		mongoDao.updateDataCollection(criteriaForDelete, deleted);
	}

	private void dispatchMessageToUsers(Domain domainData, Message message,
			List<User> listUser) throws JsonGenerationException,
			JsonMappingException, IOException {
		for (User user : listUser) {
			setFilterForAddMessage(domainData, user);
			BasicDBObject criteria = criteriaForAddMessages(domainData, user);
			BasicDBObject messageAdded = new BasicDBObject(
					Constantes.OPERATOR_PUSH, new BasicDBObject(
							ACCESS_TO_COLLECTION_LISTMESSAGES,
							(BasicDBObject) JSON.parse(message.toJSON())));
			mongoDao.updateDataCollection(criteria, messageAdded);
		}
	}

	private List<User> setReceivingUser(Domain domainData,
			Set<String> listReceptions) throws JsonGenerationException,
			JsonMappingException, IOException {
		List<User> listUser = new ArrayList<>();
		BasicDBObject domainFilter = (BasicDBObject) JSON
				.parse(new Domain.Builder()
						.idGenerado(domainData.getIdGenerado()).build()
						.toJSON());
		for (String userReception : listReceptions) {
			List<Domain> queryProjection = (List<Domain>) mongoDao
					.findJSONQueryProjection(
							Constantes.QUERY_ELEM_MATCH_USERNAME,
							new Object[] { userReception }, domainFilter);
			listUser.add(queryProjection.get(0).getFirstUser());

		}
		return listUser;

	}

	private Set<String> setReceptions(Message message) {
		Set<String> listReceptions = new HashSet<>();
		listReceptions.addAll(message.getListTo() != null ? message.getListTo()
				: new ArrayList<String>());
		listReceptions.addAll(message.getListCC() != null ? message.getListCC()
				: new ArrayList<String>());
		listReceptions.addAll(message.getListCCO() != null ? message
				.getListCCO() : new ArrayList<String>());
		return listReceptions;
	}

	private Domain validateData(Domain domainData)
			throws DomainNotExistsException, UserNotExistsException {
		Domain dtoObject = checkDomain(domainData);
		checkUserInDomain(domainData, dtoObject);
		return dtoObject;
	}

	private void setFilterForAddMessage(Domain domainData, User user) {
		Domain domainFilter = new Domain.Builder().idGenerado(
				domainData.getIdGenerado()).build();
		User userFilter = new User.Builder().idGenerado(user.getIdGenerado())
				.build();
		domainFilter.addUserToDomain(userFilter);
	}

	private void setIdGenerado(Message message) {
		String collectionMessage = Message.class.getAnnotation(
				CollectionName.class).value();
		String idGenerado = mongoDao.getSeqNumber(collectionMessage);
		message.setIdGenerado(idGenerado);
	}

	private BasicDBObject criteriaForAddMessages(Domain domainData, User user) {
		Object[] parms = new Object[] { domainData.getIdGenerado(),
				user.getIdGenerado() };
		String query = QueryUtils.getJSONQuery(Constantes.QUERY_MESSAGES,
				User.class, parms);
		BasicDBObject criteria = (BasicDBObject) JSON.parse(query);
		return criteria;
	}

	private String setDataFilterQuery(Domain domainFilter) {
		User userFilter = domainFilter.getFirstUser();
		Object[] parms = new Object[] { domainFilter.getIdGenerado(),
				userFilter.getIdGenerado() };
		String queryMessage = QueryUtils.getJSONQuery(
				Constantes.QUERY_FOR_MESSAGE, Message.class, parms);
		return queryMessage;
	}

	private void checkUserInDomain(Domain domain, Domain dtoObject)
			throws UserNotExistsException {
		if (!dtoObject.existsUserInDomain(domain.getFirstUser())) {
			throw new UserNotExistsException();
		}
	}

	private Domain checkDomain(Domain domain) throws DomainNotExistsException {
		Object[] parms = new Object[] { domain.getIdGenerado() };
		String queryIdGenerado = QueryUtils.getJSONQuery(
				Constantes.QUERY_ID_GENERADO, Domain.class, parms);
		Domain dtoObject = (Domain) mongoDao.findOneByQuery(queryIdGenerado);
		if (dtoObject == null) {
			throw new DomainNotExistsException();
		}
		return dtoObject;
	}

	private Domain setProjectionForFullMessage() {
		Domain projection = new Domain.Builder()
				.idGenerado(Constantes.DEFAULT_VALUE_PROJECTION)
				.nomDomain(Constantes.DEFAULT_VALUE_PROJECTION).build();
		User user = new User.Builder().userName(
				Constantes.DEFAULT_VALUE_PROJECTION).build();
		Message message = new Message.Build()
				.from(Constantes.DEFAULT_VALUE_PROJECTION)
				.content(Constantes.DEFAULT_VALUE_PROJECTION)
				.subject(Constantes.DEFAULT_VALUE_PROJECTION)
				.cc(Arrays
						.asList(new String[] { Constantes.DEFAULT_VALUE_PROJECTION }))
				.build();
		user.addMessage(message);
		projection.addUserToDomain(user);
		return projection;
	}

}