package org.uji.message.services.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.inject.Named;

import org.uji.message.dao.MongoDBDao;
import org.uji.message.dao.impl.MongoDBDaoImpl;
import org.uji.message.dto.Domain;
import org.uji.message.dto.MongoDTOObject;
import org.uji.message.dto.User;
import org.uji.message.exception.ContacNotExistsException;
import org.uji.message.exception.DomainNotExistsException;
import org.uji.message.services.ContactService;

@Named("contactService")
public class ContactServiceImpl implements ContactService {

	MongoDBDao<Serializable, MongoDTOObject> mongoDBDao;

	public ContactServiceImpl() {
		mongoDBDao = new MongoDBDaoImpl<String, Domain>(new Domain());
	}

	@Override
	public List<User> getAllContacts(Domain domain)
			throws DomainNotExistsException {

		try {
			Domain domainChecked = checkDomain(domain);
			if (domainChecked == null) {
				throw new DomainNotExistsException();
			}
			return domainChecked.getListUsersDomain();

		} catch (IOException e) {
			throw new DomainNotExistsException(e);
		}

	}

	@Override
	public User getContact(Domain domainFilter)
			throws DomainNotExistsException, ContacNotExistsException {
		try {
			Domain domainChecked = checkDomain(domainFilter);
			if (domainChecked == null) {
				throw new DomainNotExistsException();
			}

			User user = domainFilter.getFirstUser();
			if (domainChecked.existsUserInDomain(user)) {
				return domainChecked.getUserByIdGenerado(user);
			} else {
				throw new ContacNotExistsException();
			}

		} catch (IOException e) {
			throw new DomainNotExistsException(e);
		}
	}

	private Domain checkDomain(Domain domain) throws IOException {
		String filter = new Domain.Builder().idGenerado(domain.getIdGenerado())
				.nomDomain(domain.getNomDomain()).build().toJSON();
		Domain domainExists = (Domain) mongoDBDao.findOneByQuery(filter);
		return domainExists;
	}

}
