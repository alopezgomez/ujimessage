package org.uji.message.services.impl;

import java.io.IOException;
import java.io.Serializable;

import javax.inject.Named;

import org.uji.message.annotations.dao.CollectionName;
import org.uji.message.dao.MongoDBDao;
import org.uji.message.dao.impl.MongoDBDaoImpl;
import org.uji.message.dto.Chat;
import org.uji.message.dto.MongoDTOObject;
import org.uji.message.exception.ChatNotExistException;
import org.uji.message.exception.ChatNotInitException;
import org.uji.message.services.ChatService;

@Named("chatService")
public class ChatServiceImpl implements ChatService {

	private MongoDBDao<Serializable, MongoDTOObject> mongoDao;

	@Override
	public void initChat(Chat chat) throws ChatNotInitException {

		mongoDao = new MongoDBDaoImpl<String, Chat>(chat);
		String collectionChat = Chat.class.getAnnotation(CollectionName.class)
				.value();
		chat.setIdGenerado(mongoDao.getSeqNumber(collectionChat));
		mongoDao.insert(chat);

	}

	@Override
	public Chat getChat(Chat chat) throws ChatNotExistException {

		mongoDao = new MongoDBDaoImpl<String, Chat>(chat);

		try {
			Chat newChat = (Chat) mongoDao.findOneByQuery(chat.toJSON());
			if (newChat == null) {
				throw new ChatNotExistException();
			}
			mongoDao.delete(newChat.getIdGenerado());
			return newChat;
		} catch (IOException e) {
			throw new ChatNotExistException(e);
		}

	}

}
