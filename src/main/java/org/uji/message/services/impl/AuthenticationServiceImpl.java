package org.uji.message.services.impl;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

import javax.inject.Named;

import org.uji.message.dao.MongoDBDao;
import org.uji.message.dao.impl.MongoDBDaoImpl;
import org.uji.message.dto.Domain;
import org.uji.message.dto.MongoDTOObject;
import org.uji.message.dto.User;
import org.uji.message.exception.UserNotExistsException;
import org.uji.message.security.Token;
import org.uji.message.security.TypeSecurity;
import org.uji.message.services.AuthenticationService;
import org.uji.message.utils.Constantes;
import org.uji.message.utils.SecurityUtils;

@Named("authenticationService")
public class AuthenticationServiceImpl implements AuthenticationService {

	private MongoDBDao<Serializable, MongoDTOObject> mongoDBDao;

	public AuthenticationServiceImpl() {
		mongoDBDao = new MongoDBDaoImpl<String, Domain>(new Domain());
	}

	@Override
	public Domain existsUser(Domain domain) throws UserNotExistsException {

		Object[] parms = new Object[] { domain.getNomDomain(),
				domain.getFirstUser().getUserName() };
		List<Domain> list = (List<Domain>) mongoDBDao.findJSONQuery(
				Constantes.QUERY_USER, parms);

		if (!haveData(list)) {
			throw new UserNotExistsException();
		}
		User firstUser = domain.getFirstUser();
		try {
			Token token = SecurityUtils.getSecurityHashed(
					TypeSecurity.PASSWORD,
					new String[] { firstUser.getUserName(),
							firstUser.getPassword() });
			User userReduced = null;
			Domain domainResult = list.get(0);
			List<User> listResultUser = domainResult.getListUsersDomain();
			for (int i = 0; i < listResultUser.size() && userReduced == null; i++) {
				User userResult = listResultUser.get(i);
				if (userResult.getUserName().equals(firstUser.getUserName())
						&& token.getToken().equals(userResult.getPassword())) {
					userReduced = userResult;
				}
			}
			if (userReduced == null) {
				throw new UserNotExistsException();
			}
			domainResult.setListUsersDomain(Arrays
					.asList(new User[] { userReduced }));

			return domainResult;
		} catch (NoSuchAlgorithmException e) {
			throw new UserNotExistsException(e);
		}

	}

	private boolean haveData(List<Domain> list) {
		return list != null && !list.isEmpty();
	}

	@Override
	public boolean isAdminUser(Domain domain) throws UserNotExistsException {

		Object[] parms = new Object[] { domain.getNomDomain(),
				domain.getUserAdmin().getUserName() };
		List<Domain> list = (List<Domain>) mongoDBDao.findJSONQuery(
				Constantes.QUERY_ADMIN_USER, parms);
		if (haveData(list)) {
			return true;
		}

		return false;
	}
}
