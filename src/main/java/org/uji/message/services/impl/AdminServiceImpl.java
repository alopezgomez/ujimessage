package org.uji.message.services.impl;

import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.inject.Named;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.uji.message.annotations.dao.CollectionName;
import org.uji.message.dao.MongoDBDao;
import org.uji.message.dao.impl.MongoDBDaoImpl;
import org.uji.message.dto.Domain;
import org.uji.message.dto.MongoDTOObject;
import org.uji.message.dto.User;
import org.uji.message.exception.DomainNotExistsException;
import org.uji.message.exception.UserExistsInDomainException;
import org.uji.message.exception.UserNotExistsException;
import org.uji.message.security.Token;
import org.uji.message.security.TypeSecurity;
import org.uji.message.services.AdminService;
import org.uji.message.utils.Constantes;
import org.uji.message.utils.SecurityUtils;

import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;

@Named("adminService")
public class AdminServiceImpl implements AdminService {

	private MongoDBDao<Serializable, MongoDTOObject> mongoDAO;

	public AdminServiceImpl() {
		mongoDAO = new MongoDBDaoImpl<String, Domain>(new Domain());
	}

	@Override
	public void addUsertToDomain(Domain domain)
			throws UserExistsInDomainException {
		try {

			Domain domainExists = checkDomain(domain);

			User usersInDomain = checkUsersInDomain(domainExists, domain);
			if (usersInDomain != null) {
				throw new UserExistsInDomainException();
			}

			User user = domain.getFirstUser();

			Token token = SecurityUtils.getSecurityHashed(
					TypeSecurity.PASSWORD, new String[] { user.getUserName(),
							user.getPassword() });
			user.setPassword(token.getToken());
			String collectionUser = User.class.getAnnotation(
					CollectionName.class).value();
			user.setIdGenerado(mongoDAO.getSeqNumber(collectionUser));
			BasicDBObject domainCriteria = (BasicDBObject) JSON
					.parse(new Domain.Builder()._id(domainExists.get_id())
							.build().toJSON());
			BasicDBObject userAdded = new BasicDBObject(
					Constantes.OPERATOR_PUSH, new BasicDBObject(
							ACCES_COLLECTION_LISTUSERSDOMAIN,
							(BasicDBObject) JSON.parse(user.toJSON())));
			mongoDAO.updateDataCollection(domainCriteria, userAdded);
		} catch (IOException e) {
			throw new UserExistsInDomainException(e);
		} catch (NoSuchAlgorithmException e) {
			throw new UserExistsInDomainException(e);
		}
	}

	@Override
	public void deleteDomain(Domain domain) throws DomainNotExistsException {

		try {
			Domain domainExists = checkDomain(domain);
			if (domainExists == null) {
				throw new DomainNotExistsException();
			}
			mongoDAO.delete(domain.getIdGenerado());
		} catch (IOException e) {
			throw new DomainNotExistsException(e);
		}

	}

	@Override
	public void deleteUserInDomain(Domain domain)
			throws DomainNotExistsException, UserNotExistsException {

		try {
			Domain domainExists = checkDomain(domain);
			if (domainExists == null) {
				throw new DomainNotExistsException();
			}

			User usersInDomain = checkUsersInDomain(domainExists, domain);
			if (usersInDomain == null) {
				throw new UserNotExistsException();
			}

			BasicDBObject criteria = buildCriteria(domainExists, usersInDomain);

			BasicDBObject deleted = new BasicDBObject(Constantes.OPERATOR_PULL,
					new BasicDBObject("listUsersDomain", new BasicDBObject(
							"idGenerado", usersInDomain.getIdGenerado())));
			mongoDAO.updateDataCollection(criteria, deleted);
		} catch (IOException e) {
			throw new DomainNotExistsException(e);
		}
	}

	@Override
	public Domain getAllUsersInDomain(Domain domain)
			throws DomainNotExistsException {

		try {
			Domain domainChecked = checkDomain(domain);
			if (domainChecked == null) {
				throw new DomainNotExistsException();
			}
			return domainChecked;
		} catch (IOException e) {
			throw new DomainNotExistsException(e);
		}
	}

	@Override
	public List<Domain> getAllDomains() {

		return (List<Domain>) mongoDAO.find();
	};

	private BasicDBObject buildCriteria(Domain domainExists, User usersInDomain)
			throws JsonGenerationException, JsonMappingException, IOException {
		Domain domainCriteria = new Domain.Builder()._id(domainExists.get_id())
				.build();
		BasicDBObject criteria = (BasicDBObject) JSON.parse(domainCriteria
				.toJSON());
		return criteria;
	}

	private Domain checkDomain(Domain domain) throws IOException {
		String filter = new Domain.Builder().idGenerado(domain.getIdGenerado())
				.nomDomain(domain.getNomDomain()).build().toJSON();
		Domain domainExists = (Domain) mongoDAO.findOneByQuery(filter);
		return domainExists;
	}

	private User checkUsersInDomain(Domain domainExists, Domain domain) {
		for (User newUser : domain.getListUsersDomain()) {
			if (domainExists.existsUserInDomain(newUser)) {
				return newUser;
			}
		}
		return null;
	}

}
