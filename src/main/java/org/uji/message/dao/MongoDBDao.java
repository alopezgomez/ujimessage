package org.uji.message.dao;

import java.io.Serializable;
import java.util.List;

import org.uji.message.annotations.dao.JSONQuery;
import org.uji.message.annotations.dao.JSONQuerys;
import org.uji.message.dto.MongoDTOObject;

import com.mongodb.BasicDBObject;

/**
 * Interfaz generica para el manejo del almacenmiento de los DTO de la
 * aplicacion
 * 
 * @author angel
 * */
public interface MongoDBDao<ID extends Serializable, DTO extends MongoDTOObject> {

	static final String DB = "ujiMessage";
	static final String ATTR_ID_GENERADO = "idGenerado";

	/**
	 * Metodo que retorna todos los datos de la coleccion
	 * 
	 * @return Lista de elementos de tipo {@link MongoDTOObject}
	 * */
	List<? extends MongoDTOObject> find();

	/**
	 * Metodo que retorna el elemento de tipo {@link MongoDTOObject} a partir de
	 * su clave
	 * 
	 * @param key
	 * @return Elemento de tipo {@link MongoDTOObject}
	 * */
	DTO findOneByQuery(ID key);

	/**
	 * Metodo que realiza el guardado del elemento retorna el identifiacador del
	 * elemento en la coleccion
	 * 
	 * @param mongoObject
	 * @return identifcador
	 * */
	ID insert(DTO mongoObject);

	/**
	 * Metodo que realiza la eliminacion de un elemento en la coleccion a partir
	 * de su clave
	 * 
	 * @param key
	 * */
	void delete(ID key);

	/**
	 * Metodo que realiza operaciones de actualizacion sobre una coleccion
	 * 
	 * @param criteria
	 * @param updated
	 * 
	 * 
	 * */
	void updateDataCollection(BasicDBObject criteria, BasicDBObject updated);

	/**
	 * Metodo que realiza la busqueda a partir de una query y solamente muestra
	 * aquello que este en la proyeccion
	 * 
	 * @param query
	 * @param projection
	 * @return Lista de objetos de tipo {@link MongoDTOObject} resultado de la
	 *         query
	 * */
	List<? extends MongoDTOObject> findUsingProjection(BasicDBObject query,
			BasicDBObject projection);

	/**
	 * Metodo que realiza la busqueda a partir de una query definida con la
	 * anotacion {@link JSONQuery} o {@link JSONQuerys}
	 * 
	 * @param query
	 *            nombre de la query
	 * @param parms
	 *            objeto con los datos para realizar el filtro
	 * 
	 * @return Lista de objetos de tipo {@link MongoDTOObject} resultado de la
	 *         query
	 * */
	List<? extends MongoDTOObject> findJSONQuery(String query, Object[] parms);

	/**
	 * Metodo que realiza la busqueda a partir de una query definida con la
	 * anotacion {@link JSONQuery} o {@link JSONQuerys}
	 * 
	 * @param query
	 *            nombre de la query
	 * @param parms
	 *            objeto con los datos para realizar el filtro
	 * @param dto
	 * @return Lista de objetos de tipo {@link MongoDTOObject} resultado de la
	 *         query
	 * */
	List<? extends MongoDTOObject> findJSONQueryProjection(String query,
			Object[] parms, BasicDBObject dto);

	/**
	 * Metodo que obtiene el numero de secuencia generado para la identificacion
	 * interna de los datos de una coleccion
	 * 
	 * @param collectionName
	 *            nombre de la coleccion, este nombre debe de estar en la
	 *            coleccion secuences
	 * @return el secuencial generado
	 * */
	String getSeqNumber(String collectionName);

}
