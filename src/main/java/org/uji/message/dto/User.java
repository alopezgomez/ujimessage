package org.uji.message.dto;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.uji.message.annotations.dao.CollectionName;
import org.uji.message.annotations.dao.JSONQuery;
import org.uji.message.annotations.dao.Projection;
import org.uji.message.utils.Constantes;
import org.uji.message.utils.ProyectionType;

@CollectionName("user")
@JSONQuery(nameQuery = Constantes.QUERY_MESSAGES, query = "{idGenerado:?,listUsersDomain.idGenerado:?}")
public class User implements MongoDTOObject, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2252923491114204311L;
	private String userName;
	private String password;
	private String idGenerado;
	private List<Message> listMessages;

	@Projection
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Projection
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Projection
	public String getIdGenerado() {
		return idGenerado;
	}

	public void setIdGenerado(String idGenerado) {
		this.idGenerado = idGenerado;
	}

	@Projection(ProyectionType.LIST)
	public List<Message> getListMessages() {
		return listMessages;
	}

	public void setListMessages(List<Message> listMessages) {
		this.listMessages = listMessages;
	}

	@JsonIgnore
	public Message getFirstMessage() {
		return this.listMessages != null ? this.listMessages
				.get(Constantes.FIRS_RESULT) : new Message();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idGenerado == null) ? 0 : idGenerado.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime * result
				+ ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		User other = (User) obj;
		if (idGenerado == null) {
			if (other.idGenerado != null) {
				return false;
			}
		} else if (!idGenerado.equals(other.idGenerado)) {
			return false;
		}

		if (userName == null) {
			if (other.userName != null) {
				return false;
			}
		} else if (!userName.equals(other.userName)) {
			return false;
		}
		return true;
	}

	@Override
	public String toJSON() throws JsonGenerationException,
			JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Inclusion.NON_NULL);
		return mapper.writeValueAsString(this);
	}

	@Override
	public MongoDTOObject toObject(String json) throws JsonParseException,
			JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(json, User.class);
	}

	public void addMessage(Message message) {
		if (listMessages == null) {
			listMessages = new ArrayList<>();
		}
		listMessages.add(message);

	}

	public static class Builder {

		private String userName;
		private String password;
		private String idGenerado;
		private List<Message> listMessages;

		public Builder userName(String userName) {
			this.userName = userName;
			return this;
		}

		public Builder password(String password) {
			this.password = password;
			return this;
		}

		public Builder idGenerado(String idGenerado) {
			this.idGenerado = idGenerado;
			return this;
		}

		public Builder messages(List<Message> listMessages) {
			this.listMessages = listMessages;
			return this;
		}

		public User build() {
			User user = new User();
			user.setIdGenerado(idGenerado);
			user.setListMessages(listMessages);
			user.setPassword(password);
			user.setUserName(userName);
			return user;
		}

	}

}