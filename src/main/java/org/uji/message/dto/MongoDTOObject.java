package org.uji.message.dto;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.uji.message.annotations.dao.JSONQuery;
import org.uji.message.utils.Constantes;

/**
 * Interfaz para las implementaciones de los objetos de persistencia
 * 
 * @author angel
 * */
@JSONQuery(nameQuery = Constantes.SEQUENCES, query = "function getNextVal(){var ret= db.sequences.findAndModify({"
		+ "	query : {_id: ? },"
		+ "	update :{ $inc:{ seq:1} },"
		+ "	new : true"
		+ "});" + "return ret.seq.toString();}")
public interface MongoDTOObject {

	/**
	 * Metodo que realiza la conversion de las propiedades en formato JSON
	 * 
	 * @return Cadena en formato JSON
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 * */
	String toJSON() throws JsonGenerationException, JsonMappingException,
			IOException;

	/**
	 * Metodo que realiza la deserializacion de la cadena json a objeto
	 * 
	 * @param json
	 * @return objeto deserializado
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 * */
	MongoDTOObject toObject(String json) throws JsonParseException,
			JsonMappingException, IOException;

}
