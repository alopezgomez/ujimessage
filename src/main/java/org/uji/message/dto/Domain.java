package org.uji.message.dto;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.uji.message.annotations.dao.CollectionName;
import org.uji.message.annotations.dao.JSONQuery;
import org.uji.message.annotations.dao.JSONQuerys;
import org.uji.message.annotations.dao.Projection;
import org.uji.message.utils.Constantes;
import org.uji.message.utils.ProyectionType;

@CollectionName("domain")
@JSONQuerys({
		@JSONQuery(nameQuery = Constantes.QUERY_ADMIN, query = "{nomDomain:?,userAdmin.idGenerado:?}"),
		@JSONQuery(nameQuery = Constantes.QUERY_ADMIN_USER, query = "{nomDomain:?,userAdmin.userName:?}"),
		@JSONQuery(nameQuery = Constantes.QUERY_USER, query = "{nomDomain:?,listUsersDomain.userName:?}"),
		@JSONQuery(nameQuery = Constantes.QUERY_ID_GENERADO, query = "{idGenerado:?}"),
		@JSONQuery(nameQuery = Constantes.QUERY_ELEM_MATCH_USERNAME, query = "{listUsersDomain:{$elemMatch:{userName:?}}}"),
		@JSONQuery(nameQuery = Constantes.QUERY_ELEM_MATCH, query = "{listUsersDomain:{$elemMatch:{idGenerado:?}}}") })
public class Domain implements MongoDTOObject, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9056478732053859033L;
	private String nomDomain;
	private MongoID _id;
	private String idGenerado;
	private User userAdmin;
	private List<User> listUsersDomain;

	@Projection
	public String getNomDomain() {
		return nomDomain;
	}

	public void setNomDomain(String nomDomain) {
		this.nomDomain = nomDomain;
	}

	public MongoID get_id() {
		return _id;
	}

	public void set_id(MongoID _id) {
		this._id = _id;
	}

	@Projection
	public String getIdGenerado() {
		return idGenerado;
	}

	public void setIdGenerado(String idGenerado) {
		this.idGenerado = idGenerado;
	}

	@Projection(ProyectionType.SIMPLE)
	public User getUserAdmin() {
		return userAdmin;
	}

	public void setUserAdmin(User userAdmin) {
		this.userAdmin = userAdmin;
	}

	@Projection(ProyectionType.LIST)
	public List<User> getListUsersDomain() {
		return listUsersDomain;
	}

	public void setListUsersDomain(List<User> listUsersDomain) {
		this.listUsersDomain = listUsersDomain;
	}

	@JsonIgnore
	public User getFirstUser() {
		return this.listUsersDomain != null ? this.listUsersDomain
				.get(Constantes.FIRS_RESULT) : new User();
	}

	@JsonIgnore
	public User getUserByIdGenerado(User user) {
		for (User userInDomain : listUsersDomain) {
			if (StringUtils.equals(user.getIdGenerado(),
					userInDomain.getIdGenerado())) {
				return userInDomain;
			}
		}
		return new User();
	}

	public void addUserToDomain(User user) {
		initListUsers();
		listUsersDomain.add(user);
	}

	public void addUsersToUsersDomain(List<User> listUsers) {
		initListUsers();
		listUsersDomain.addAll(listUsers);
	}

	private void initListUsers() {
		if (listUsersDomain == null) {
			listUsersDomain = new ArrayList<>();
		}
	}

	@JsonIgnore
	public boolean existsUserInDomain(User user) {
		for (User userInDomain : listUsersDomain) {

			if (StringUtils.equals(user.getUserName(),
					userInDomain.getUserName())
					|| StringUtils.equals(user.getIdGenerado(),
							userInDomain.getIdGenerado())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String toJSON() throws JsonGenerationException,
			JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Inclusion.NON_NULL);
		return mapper.writeValueAsString(this);
	}

	@Override
	public MongoDTOObject toObject(String json) throws JsonParseException,
			JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		return mapper.readValue(json, Domain.class);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_id == null) ? 0 : _id.hashCode());
		result = prime * result
				+ ((idGenerado == null) ? 0 : idGenerado.hashCode());
		result = prime * result
				+ ((nomDomain == null) ? 0 : nomDomain.hashCode());
		result = prime * result
				+ ((userAdmin == null) ? 0 : userAdmin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Domain other = (Domain) obj;
		if (_id == null) {
			if (other._id != null) {
				return false;
			}
		} else if (!_id.equals(other._id)) {
			return false;
		}
		if (idGenerado == null) {
			if (other.idGenerado != null) {
				return false;
			}
		} else if (!idGenerado.equals(other.idGenerado)) {
			return false;
		}
		if (nomDomain == null) {
			if (other.nomDomain != null) {
				return false;
			}
		} else if (!nomDomain.equals(other.nomDomain)) {
			return false;
		}
		if (userAdmin == null) {
			if (other.userAdmin != null) {
				return false;
			}
		} else if (!userAdmin.equals(other.userAdmin)) {
			return false;
		}
		return true;
	}

	public static class Builder {
		private String nomDomain;
		private MongoID _id;
		private String idGenerado;
		private User userAdmin;
		private List<User> listUsersDomain;

		public Builder nomDomain(String nomDomain) {
			this.nomDomain = nomDomain;
			return this;
		}

		public Builder _id(MongoID _id) {
			this._id = _id;
			return this;
		}

		public Builder idGenerado(String idGenerado) {
			this.idGenerado = idGenerado;
			return this;
		}

		public Builder userAdmin(User userAdmin) {
			this.userAdmin = userAdmin;
			return this;
		}

		public Builder usersDomain(List<User> listUsersDomain) {
			this.listUsersDomain = listUsersDomain;
			return this;
		}

		public Domain build() {
			Domain domain = new Domain();
			domain.set_id(_id);
			domain.setIdGenerado(idGenerado);
			domain.setListUsersDomain(listUsersDomain);
			domain.setNomDomain(nomDomain);
			domain.setUserAdmin(userAdmin);
			return domain;
		}

	}

}
