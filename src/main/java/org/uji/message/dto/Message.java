package org.uji.message.dto;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.uji.message.annotations.dao.CollectionName;
import org.uji.message.annotations.dao.JSONQuery;
import org.uji.message.annotations.dao.JSONQuerys;
import org.uji.message.annotations.dao.Projection;
import org.uji.message.utils.Constantes;

@CollectionName("message")
@JSONQuerys({
		@JSONQuery(nameQuery = Constantes.QUERY_FOR_MESSAGE, query = "{ idGenerado: ? , listUsersDomain.idGenerado: ? }"),
		@JSONQuery(nameQuery = Constantes.QUERY_ALL_MESSAGE, query = "{listUsersDomain.listMessages.listTo:{$in:[?]}}") })
public class Message implements MongoDTOObject, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6340601844717951228L;
	private String idGenerado;
	private String subject;
	private String from;
	private List<String> listTo;
	private List<String> listCC;
	private List<String> listCCO;
	private String content;

	@Projection
	public String getIdGenerado() {
		return idGenerado;
	}

	public void setIdGenerado(String idGenerado) {
		this.idGenerado = idGenerado;
	}

	@Projection
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Projection
	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	@Projection
	public List<String> getListTo() {
		return listTo;
	}

	public void setListTo(List<String> listTo) {
		this.listTo = listTo;
	}

	@Projection
	public List<String> getListCC() {
		return listCC;
	}

	public void setListCC(List<String> listCC) {
		this.listCC = listCC;
	}

	@Projection
	public List<String> getListCCO() {
		return listCCO;
	}

	public void setListCCO(List<String> listCCO) {
		this.listCCO = listCCO;
	}

	@Projection
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toJSON() throws JsonGenerationException,
			JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Inclusion.NON_NULL);
		return mapper.writeValueAsString(this);
	}

	@Override
	public MongoDTOObject toObject(String json) throws JsonParseException,
			JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(json, User.class);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((content == null) ? 0 : content.hashCode());
		result = prime * result + ((from == null) ? 0 : from.hashCode());
		result = prime * result
				+ ((idGenerado == null) ? 0 : idGenerado.hashCode());
		result = prime * result + ((listCC == null) ? 0 : listCC.hashCode());
		result = prime * result + ((listCCO == null) ? 0 : listCCO.hashCode());
		result = prime * result + ((listTo == null) ? 0 : listTo.hashCode());
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Message other = (Message) obj;
		if (content == null) {
			if (other.content != null) {
				return false;
			}
		} else if (!content.equals(other.content)) {
			return false;
		}
		if (from == null) {
			if (other.from != null) {
				return false;
			}
		} else if (!from.equals(other.from)) {
			return false;
		}
		if (idGenerado == null) {
			if (other.idGenerado != null) {
				return false;
			}
		} else if (!idGenerado.equals(other.idGenerado)) {
			return false;
		}
		if (listCC == null) {
			if (other.listCC != null) {
				return false;
			}
		} else if (!listCC.equals(other.listCC)) {
			return false;
		}
		if (listCCO == null) {
			if (other.listCCO != null) {
				return false;
			}
		} else if (!listCCO.equals(other.listCCO)) {
			return false;
		}
		if (listTo == null) {
			if (other.listTo != null) {
				return false;
			}
		} else if (!listTo.equals(other.listTo)) {
			return false;
		}
		if (subject == null) {
			if (other.subject != null) {
				return false;
			}
		} else if (!subject.equals(other.subject)) {
			return false;
		}
		return true;
	}

	public static class Build {

		private String idGenerado;
		private String subject;
		private String from;
		private List<String> listTo;
		private List<String> listCC;
		private List<String> listCCO;
		private String content;

		public Build idGenerado(String idGenerado) {
			this.idGenerado = idGenerado;
			return this;
		}

		public Build subject(String subject) {
			this.subject = subject;
			return this;
		}

		public Build from(String from) {
			this.from = from;
			return this;
		}

		public Build to(List<String> listTo) {
			this.listTo = listTo;
			return this;
		}

		public Build cc(List<String> listCC) {
			this.listCC = listCC;
			return this;
		}

		public Build cco(List<String> listCCO) {
			this.listCCO = listCCO;
			return this;
		}

		public Build content(String content) {
			this.content = content;
			return this;
		}

		public Message build() {
			Message message = new Message();
			message.setContent(content);
			message.setFrom(from);
			message.setIdGenerado(idGenerado);
			message.setListCC(listCC);
			message.setListCCO(listCCO);
			message.setListTo(listTo);
			message.setSubject(subject);
			return message;
		}

	}

}