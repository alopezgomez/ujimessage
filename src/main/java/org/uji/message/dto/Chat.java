package org.uji.message.dto;

import java.io.IOException;
import java.io.Serializable;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.uji.message.annotations.dao.CollectionName;

@CollectionName("chat")
public class Chat implements MongoDTOObject, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4412516760373742409L;

	private MongoID _id;

	private String userName;
	private String domainName;
	private String userNameDest;
	private String idGenerado;

	public MongoID get_id() {
		return _id;
	}

	public void set_id(MongoID _id) {
		this._id = _id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getUserNameDest() {
		return userNameDest;
	}

	public void setUserNameDest(String userNameDest) {
		this.userNameDest = userNameDest;
	}

	public String getIdGenerado() {
		return idGenerado;
	}

	public void setIdGenerado(String idGenerado) {
		this.idGenerado = idGenerado;
	}

	@Override
	public String toJSON() throws JsonGenerationException,
			JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Inclusion.NON_NULL);
		return mapper.writeValueAsString(this);

	}

	@Override
	public MongoDTOObject toObject(String json) throws JsonParseException,
			JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(json, Chat.class);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_id == null) ? 0 : _id.hashCode());
		result = prime * result
				+ ((domainName == null) ? 0 : domainName.hashCode());
		result = prime * result
				+ ((idGenerado == null) ? 0 : idGenerado.hashCode());
		result = prime * result
				+ ((userName == null) ? 0 : userName.hashCode());
		result = prime * result
				+ ((userNameDest == null) ? 0 : userNameDest.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Chat other = (Chat) obj;
		if (_id == null) {
			if (other._id != null)
				return false;
		} else if (!_id.equals(other._id))
			return false;
		if (domainName == null) {
			if (other.domainName != null)
				return false;
		} else if (!domainName.equals(other.domainName))
			return false;
		if (idGenerado == null) {
			if (other.idGenerado != null)
				return false;
		} else if (!idGenerado.equals(other.idGenerado))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		if (userNameDest == null) {
			if (other.userNameDest != null)
				return false;
		} else if (!userNameDest.equals(other.userNameDest))
			return false;
		return true;
	}

	public static class Builder {

		private MongoID _id;

		private String userName;
		private String domainName;
		private String userNameDest;
		private String idGenerado;

		public Builder id(MongoID _id) {
			this._id = _id;
			return this;
		}

		public Builder userName(String userName) {
			this.userName = userName;
			return this;
		}

		public Builder domainName(String domainName) {
			this.domainName = domainName;
			return this;
		}

		public Builder userNameDest(String userNameDest) {
			this.userNameDest = userNameDest;
			return this;
		}

		public Builder idGenerado(String idGenerado) {
			this.idGenerado = idGenerado;
			return this;
		}

		public Chat build() {
			Chat chat = new Chat();
			chat.set_id(_id);
			chat.setDomainName(domainName);
			chat.setIdGenerado(idGenerado);
			chat.setUserName(userName);
			chat.setUserNameDest(userNameDest);
			return chat;
		}
	}
}