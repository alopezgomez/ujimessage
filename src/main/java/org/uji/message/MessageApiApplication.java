package org.uji.message;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.uji.message.annotations.security.Secured;

/**
 * Clase que representa el path de inicio para los servicios rest En esta clase
 * se definen los filtros de seguridad dados por {@link Secured}
 * 
 * @author angel
 * */
@ApplicationPath("api")
@Secured
public class MessageApiApplication extends Application {

}
