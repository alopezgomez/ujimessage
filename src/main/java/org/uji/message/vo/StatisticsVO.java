package org.uji.message.vo;

import java.io.Serializable;

public class StatisticsVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1694977830325169334L;

	private String name;
	private String value;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
