package org.uji.message.vo;

import java.io.Serializable;

public class TableDomainVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8859012511808540433L;

	private String nomDomin;
	private int numUsers;
	private int numMessages;

	public String getNomDomin() {
		return nomDomin;
	}

	public void setNomDomin(String nomDomin) {
		this.nomDomin = nomDomin;
	}

	public int getNumUsers() {
		return numUsers;
	}

	public void setNumUsers(int numUsers) {
		this.numUsers = numUsers;
	}

	public int getNumMessages() {
		return numMessages;
	}

	public void setNumMessages(int numMessages) {
		this.numMessages = numMessages;
	}

}
