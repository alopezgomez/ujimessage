package org.uji.message.input;

import java.io.Serializable;

public class MessageInput implements InputObject, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8465229172465705331L;

	private String idDomain;
	private String idUser;
	private String userName;
	private String idMessage;
	private String content;
	private String subject;
	private String[] destinations;
	private String[] destinationsInCopy;
	private String[] destinationsHiddens;
	private String destinationsStr;
	private String destinatiosInCopyStr;

	public String getIdDomain() {
		return idDomain;
	}

	public void setIdDomain(String idDomain) {
		this.idDomain = idDomain;
	}

	public String getIdUser() {
		return idUser;
	}

	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getIdMessage() {
		return idMessage;
	}

	public void setIdMessage(String idMessage) {
		this.idMessage = idMessage;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String[] getDestinations() {
		return destinations;
	}

	public void setDestinations(String[] destinations) {
		this.destinations = destinations;
	}

	public String[] getDestinationsInCopy() {
		return destinationsInCopy;
	}

	public void setDestinationsInCopy(String[] destinationsInCopy) {
		this.destinationsInCopy = destinationsInCopy;
	}

	public String[] getDestinationsHiddens() {
		return destinationsHiddens;
	}

	public void setDestinationsHiddens(String[] destinationsHiddens) {
		this.destinationsHiddens = destinationsHiddens;
	}

	public String getDestinationsStr() {
		return destinationsStr;
	}

	public void setDestinationsStr(String destinationsStr) {
		this.destinationsStr = destinationsStr;
	}

	public String getDestinatiosInCopyStr() {
		return destinatiosInCopyStr;
	}

	public void setDestinatiosInCopyStr(String destinatiosInCopyStr) {
		this.destinatiosInCopyStr = destinatiosInCopyStr;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idDomain == null) ? 0 : idDomain.hashCode());
		result = prime * result
				+ ((idMessage == null) ? 0 : idMessage.hashCode());
		result = prime * result + ((idUser == null) ? 0 : idUser.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MessageInput other = (MessageInput) obj;
		if (idDomain == null) {
			if (other.idDomain != null)
				return false;
		} else if (!idDomain.equals(other.idDomain))
			return false;
		if (idMessage == null) {
			if (other.idMessage != null)
				return false;
		} else if (!idMessage.equals(other.idMessage))
			return false;
		if (idUser == null) {
			if (other.idUser != null)
				return false;
		} else if (!idUser.equals(other.idUser))
			return false;
		return true;
	}

	public static class Build {

		private String idDomain;
		private String idUser;
		private String userName;
		private String idMessage;
		private String content;
		private String subject;
		private String[] destinations;
		private String[] destinationsInCopy;
		private String[] destinationsHiddens;
		private String destinationsStr;
		private String destinatiosInCopyStr;

		public Build idDomain(String idDomain) {
			this.idDomain = idDomain;
			return this;
		}

		public Build idUser(String idUser) {
			this.idUser = idUser;
			return this;
		}

		public Build userName(String userName) {
			this.userName = userName;
			return this;
		}

		public Build idMessage(String idMessage) {
			this.idMessage = idMessage;
			return this;
		}

		public Build content(String content) {
			this.content = content;
			return this;
		}

		public Build subject(String subject) {
			this.subject = subject;
			return this;
		}

		public Build destinations(String[] destinations) {
			this.destinations = destinations;
			return this;
		}

		public Build destinationsInCopy(String[] destinationsInCopy) {
			this.destinationsInCopy = destinationsInCopy;
			return this;
		}

		public Build destinationsHiddens(String[] destinationsHiddens) {
			this.destinationsHiddens = destinationsHiddens;
			return this;
		}

		public Build destinationsStr(String destinationsStr) {
			this.destinationsStr = destinationsStr;
			return this;
		}

		public Build destinatiosInCopyStr(String destinatiosInCopyStr) {
			this.destinatiosInCopyStr = destinatiosInCopyStr;
			return this;
		}

		public MessageInput build() {
			MessageInput messageInput = new MessageInput();
			messageInput.setContent(content);
			messageInput.setDestinations(destinations);
			messageInput.setDestinationsHiddens(destinationsHiddens);
			messageInput.setDestinationsInCopy(destinationsInCopy);
			messageInput.setDestinationsStr(destinationsStr);
			messageInput.setDestinatiosInCopyStr(destinatiosInCopyStr);
			messageInput.setIdDomain(idDomain);
			messageInput.setIdMessage(idMessage);
			messageInput.setIdUser(idUser);
			messageInput.setSubject(subject);
			messageInput.setUserName(userName);
			return messageInput;

		}

	}

}
