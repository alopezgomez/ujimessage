package org.uji.message.input.chat;

import java.io.Serializable;

public class ChatInput implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4881676366749461410L;

	private String userName;
	private String domainName;
	private String userNameDest;
	private String messageContent;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getUserNameDest() {
		return userNameDest;
	}

	public void setUserNameDest(String userNameDest) {
		this.userNameDest = userNameDest;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

}
