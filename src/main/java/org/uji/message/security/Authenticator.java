package org.uji.message.security;

import java.io.Serializable;
import java.security.Principal;
import java.util.List;

import javax.ws.rs.core.SecurityContext;

import org.uji.message.dao.MongoDBDao;
import org.uji.message.dao.impl.MongoDBDaoImpl;
import org.uji.message.dto.Domain;
import org.uji.message.dto.MongoDTOObject;
import org.uji.message.dto.User;
import org.uji.message.utils.Constantes;

/**
 * Clase que implementa el {@link SecurityContext} de JAX-RS para crear roles en
 * la aplicacion
 * 
 * @author angel
 * */
public class Authenticator implements SecurityContext {

	private Principal principal;

	public Authenticator(final String userName, final String domain) {
		principal = new Principal() {
			@Override
			public String getName() {
				return userName + ":" + domain;
			}
		};
	}

	@Override
	public String getAuthenticationScheme() {
		return "";
	}

	@Override
	public Principal getUserPrincipal() {
		return principal;
	}

	@Override
	public boolean isSecure() {
		return false;
	}

	@Override
	public boolean isUserInRole(String role) {
		if (role.equals("admin")) {
			MongoDBDao<Serializable, MongoDTOObject> mongo = new MongoDBDaoImpl<String, Domain>(
					new Domain());
			Domain domain = new Domain();
			domain.setNomDomain(principal.getName().split(":")[1]);
			User userAdmin = new User();
			userAdmin.setUserName(principal.getName().split(":")[0]);
			domain.setUserAdmin(userAdmin);
			Object[] parms = new Object[] { domain.getNomDomain(),
					domain.getUserAdmin().getUserName() };
			List<Domain> list = (List<Domain>) mongo.findJSONQuery(
					Constantes.QUERY_ADMIN_USER, parms);

			if (list != null && !list.isEmpty()) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

}
