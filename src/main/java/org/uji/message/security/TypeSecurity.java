package org.uji.message.security;

import java.math.BigInteger;
import java.security.MessageDigest;

/**
 * Enumeracion implementada a modo de estategia para realizar los diferentes
 * tipos de token para la gestion de la seguridad
 * 
 * @author angel
 * */
public enum TypeSecurity {

	PASSWORD() {
		@Override
		public Token generateHashedSecurity(String[] parms, String secretKey,
				MessageDigest messageDigest) {
			String stringToHash = new String(parms[0] + secretKey + parms[1]);
			Token token = new Token();
			token.setToken(createStringHashed(messageDigest, stringToHash));
			return token;

		}
	},
	SESSION_TOKEN() {
		@Override
		public Token generateHashedSecurity(String[] parms, String secretKey,
				MessageDigest messageDigest) {
			String stringToHash = new String(parms[0] + parms[1] + secretKey
					+ parms[2]);
			Token token = new Token();
			token.setToken(createStringHashed(messageDigest, stringToHash));
			token.setDateCreation(parms[2]);

			return token;
		}
	};

	public abstract Token generateHashedSecurity(String[] parms,
			String secretKey, MessageDigest messageDigest);

	private static String createStringHashed(MessageDigest messageDigest,
			String stringToHash) {
		messageDigest.update(stringToHash.getBytes(), 0, stringToHash.length());
		return new BigInteger(1, messageDigest.digest()).toString(16);
	}

}
