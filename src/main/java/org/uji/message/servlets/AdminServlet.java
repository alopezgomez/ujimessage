package org.uji.message.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.uji.message.dto.Domain;
import org.uji.message.dto.User;
import org.uji.message.services.AdminService;
import org.uji.message.vo.TableDomainVO;

/**
 * Servlet de control de acceso, el usuario y la contraseña son por defecto
 * uji/uji
 * 
 * @author angel
 * */
@WebServlet("/administration")
public class AdminServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4037448580566511386L;
	private String CREDENTIAL_DEFAULT = "uji";

	@Inject
	private AdminService adminService;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String userName = req.getParameter("userLogin");
		String userPass = req.getParameter("userPass");
		RequestDispatcher rd = null;
		if (CREDENTIAL_DEFAULT.equals(userName)
				&& CREDENTIAL_DEFAULT.equals(userPass)) {
			List<Domain> listAll = adminService.getAllDomains();
			List<TableDomainVO> listSalida = new ArrayList<>();
			for (Domain domain : listAll) {
				TableDomainVO tableDomainVO = new TableDomainVO();
				tableDomainVO.setNomDomin(domain.getNomDomain());
				tableDomainVO.setNumUsers(domain.getListUsersDomain().size());
				int sizeMessage = 0;
				for (User user : domain.getListUsersDomain()) {
					if (user.getListMessages() != null) {
						sizeMessage += user.getListMessages().size();
					}
				}
				tableDomainVO.setNumMessages(sizeMessage);
				listSalida.add(tableDomainVO);
			}
			req.setAttribute("listData", listSalida);
			rd = getServletContext().getRequestDispatcher("/admin.jsp");
		} else {
			rd = getServletContext().getRequestDispatcher("/index.html");
		}
		rd.forward(req, resp);

	}

}
