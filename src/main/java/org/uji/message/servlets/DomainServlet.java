package org.uji.message.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.uji.message.dto.Domain;
import org.uji.message.services.AdminService;
import org.uji.message.vo.StatisticsVO;

@WebServlet("/domain")
public class DomainServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8604513946777927862L;
	@Inject
	private AdminService adminService;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		List<Domain> listAll = adminService.getAllDomains();
		List<StatisticsVO> listSalida = new ArrayList<>();
		for (Domain domain : listAll) {
			StatisticsVO statisticsVO = new StatisticsVO();
			statisticsVO.setName(domain.getNomDomain());
			statisticsVO.setValue(String.valueOf(domain.getListUsersDomain()
					.size()));
			listSalida.add(statisticsVO);
		}
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(listSalida.toArray());
		resp.setContentType("application/json");
		resp.getWriter().write(json);

	}

}
