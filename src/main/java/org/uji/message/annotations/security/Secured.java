package org.uji.message.annotations.security;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.ws.rs.NameBinding;

/**
 * Anotacion que marca que el acceso esta controlado por un token de seguridad
 * Esta anotacion se aplicara a todo el contexto del API Su implementacion
 * estara defindia en {@link org.uji.message.filters.SecurityFilter}
 * 
 * @author angel
 * */
@NameBinding
@Documented
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface Secured {

}
