package org.uji.message.annotations.dao;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.uji.message.dto.MongoDTOObject;

/**
 * Anotacion que permite que un objeto de tipo {@link MongoDTOObject} tenga
 * varias querys de tipos {@link JSONQuery}
 * 
 * @author angel
 * */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface JSONQuerys {

	JSONQuery[] value();
}
