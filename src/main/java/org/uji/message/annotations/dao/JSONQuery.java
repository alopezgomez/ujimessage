package org.uji.message.annotations.dao;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.uji.message.dto.MongoDTOObject;

/**
 * Anotacion que permite definir en los objetos {@link MongoDTOObject} querys en
 * formato JSON predeterminadas
 * 
 * @author angel
 * 
 * */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface JSONQuery {

	String nameQuery() default "";

	String query() default "";

}
