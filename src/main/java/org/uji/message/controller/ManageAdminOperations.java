package org.uji.message.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.uji.message.dto.Domain;
import org.uji.message.dto.User;
import org.uji.message.exception.DomainNotExistsException;
import org.uji.message.exception.UserExistsInDomainException;
import org.uji.message.exception.UserNotExistsException;
import org.uji.message.input.UserInput;
import org.uji.message.services.AdminService;

/**
 * Servicio REST que controla las operaciones de administracion sobre el dominio
 * Todos los metodos de esta clase estan protegidos contra accesos mediante
 * {@link SecurityContext} cuya implementacion se encuentra en
 * {@link org.uji.message.security.Authenticator}
 * 
 * @author angel
 * */
@Path("/admin")
public class ManageAdminOperations {

	@Inject
	private AdminService adminService;

	@DELETE
	@Path("/{id}")
	public Response deleteDomain(@Context SecurityContext sc,
			@PathParam("id") String idDomain) {

		if (!sc.isUserInRole("admin")) {
			return Response.status(Status.FORBIDDEN).build();
		}

		Domain domainDelete = new Domain.Builder().idGenerado(idDomain).build();
		try {
			adminService.deleteDomain(domainDelete);
		} catch (DomainNotExistsException e) {
			return Response.serverError().entity(e.getMessage()).build();
		}

		return Response.ok().build();

	}

	@DELETE
	@Path("/{id}/user/{idUser}")
	public Response deleteUser(@Context SecurityContext sc,
			@PathParam("id") String idDomain, @PathParam("idUser") String idUser) {
		if (!sc.isUserInRole("admin")) {
			return Response.status(Status.FORBIDDEN).build();
		}

		Domain domain = new Domain.Builder().idGenerado(idDomain).build();
		domain.addUserToDomain(new User.Builder().idGenerado(idUser).build());
		try {
			adminService.deleteUserInDomain(domain);
		} catch (DomainNotExistsException | UserNotExistsException e) {
			return Response.serverError().entity(e.getMessage()).build();
		}

		return Response.ok().build();

	}

	@POST
	@Path("/{id}/user")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response userInDomain(@Context SecurityContext sc,
			@PathParam("id") String idDomain, UserInput userInput) {
		if (!sc.isUserInRole("admin")) {
			return Response.status(Status.FORBIDDEN).build();
		}

		if (idDomain.equals(userInput.getIdDomain())) {
			try {
				Domain domain = new Domain.Builder().idGenerado(idDomain)
						.build();
				domain.addUserToDomain(new User.Builder()
						.userName(userInput.getUserName())
						.password(userInput.getPassword()).build());
				adminService.addUsertToDomain(domain);
			} catch (UserExistsInDomainException e) {
				return Response.serverError().entity(e.getMessage()).build();
			}
		} else {
			return Response
					.status(Status.UNAUTHORIZED)
					.entity("No puedes realizar operaciones sobre los usuarios de otro dominio")
					.build();
		}

		return Response.ok().build();

	}

	@GET
	@Path("/{id}/user")
	@Produces(MediaType.APPLICATION_JSON)
	public Response allUsersInDomain(@Context SecurityContext sc,
			@PathParam("id") String idDomain) {
		if (!sc.isUserInRole("admin")) {
			return Response.status(Status.FORBIDDEN).build();
		}

		try {
			Domain allUsersInDomain = adminService
					.getAllUsersInDomain(new Domain.Builder().idGenerado(
							idDomain).build());
			List<UserInput> listResult = new ArrayList<>();
			for (User user : allUsersInDomain.getListUsersDomain()) {
				UserInput userInput = new UserInput();
				userInput.setUserName(user.getUserName());
				userInput.setIdUser(user.getIdGenerado());
				listResult.add(userInput);
			}

			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Inclusion.NON_NULL);
			String json = mapper.writeValueAsString(listResult);
			return Response.ok(json).build();
		} catch (DomainNotExistsException | IOException e) {
			return Response.serverError().entity(e.getMessage()).build();
		}

	}
}
