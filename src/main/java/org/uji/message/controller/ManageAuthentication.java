package org.uji.message.controller;

import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.uji.message.dto.Domain;
import org.uji.message.dto.User;
import org.uji.message.exception.UserNotExistsException;
import org.uji.message.input.UserInput;
import org.uji.message.security.Token;
import org.uji.message.security.TypeSecurity;
import org.uji.message.services.AuthenticationService;
import org.uji.message.utils.Constantes;
import org.uji.message.utils.SecurityUtils;

/**
 * Servicio REST que controla las operaciones de autenticacion sobre el dominio
 * Esta clase pone en la cabecera HTTP los parametros necesarios para que el
 * cliente pueda acceder a los diferentes servicios
 * 
 * @author angel
 * */
@Path("/auth")
public class ManageAuthentication {

	@Inject
	private AuthenticationService authenticationService;

	@POST
	@Path("/token")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON)
	public Response obtainToken(UserInput userInput) {

		try {
			Domain domain = new Domain.Builder().nomDomain(
					userInput.getDomainName()).build();
			domain.addUserToDomain(new User.Builder()
					.password(userInput.getPassword())
					.userName(userInput.getUserName()).build());
			Domain existsUser = authenticationService.existsUser(domain);
			userInput.setIdDomain(existsUser.getIdGenerado());
			userInput.setIdUser(existsUser.getFirstUser().getIdGenerado());
			return generateResponseWithToken(userInput);
		} catch (NoSuchAlgorithmException | UserNotExistsException e) {
			return Response.serverError().entity(e.getMessage()).build();
		}

	}

	private Response generateResponseWithToken(UserInput user)
			throws NoSuchAlgorithmException {

		String timeCreated = String.valueOf(new Date().getTime());

		String[] parms = { user.getDomainName(), user.getUserName(),
				timeCreated };
		Token token = SecurityUtils.getSecurityHashed(
				TypeSecurity.SESSION_TOKEN, parms);
		user.setPassword(null);
		return Response
				.ok(user)
				.header(Constantes.TOKEN, token.getToken())
				.header(Constantes.CREATED_DATA, token.getDateCreation())
				.header(Constantes.USER_ALLOW,
						user.getUserName() + ":" + user.getDomainName())
				.build();
	}
}
