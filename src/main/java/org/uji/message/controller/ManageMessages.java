package org.uji.message.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.uji.message.dto.Domain;
import org.uji.message.dto.Message;
import org.uji.message.dto.User;
import org.uji.message.exception.DomainNotExistsException;
import org.uji.message.exception.MessageNotFoundException;
import org.uji.message.exception.UserNotExistsException;
import org.uji.message.input.MessageInput;
import org.uji.message.services.MessageService;

/**
 * Servicio REST que controla las operaciones de los mensajes sobre el dominio
 * 
 * @author angel
 * */
@Path("/message")
public class ManageMessages {

	@Inject
	private MessageService messageService;

	@GET
	@Path("/{idDomain}/{idUser}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response readAllMessages(@PathParam("idDomain") String idDomain,
			@PathParam("idUser") String idUser) {
		Domain domain = new Domain.Builder().idGenerado(idDomain).build();
		domain.addUserToDomain(new User.Builder().idGenerado(idUser).build());
		try {
			List<Domain> allMessages = messageService.getAllMessages(domain);
			List<MessageInput> listSalida = new ArrayList<>();
			if (!allMessages.isEmpty()) {
				Domain domainMessages = allMessages.get(0);
				User firstUser = domainMessages.getFirstUser();
				if (firstUser.getListMessages() != null) {
					for (Message message : firstUser.getListMessages()) {
						MessageInput messageInput = new MessageInput.Build()
								.content(message.getContent())
								.destinationsStr(message.getFrom())
								.idDomain(domainMessages.getIdGenerado())
								.idMessage(message.getIdGenerado())
								.idUser(firstUser.getIdGenerado())
								.subject(message.getSubject()).build();
						listSalida.add(messageInput);
					}
				}
			}
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Inclusion.NON_NULL);
			String json = mapper.writeValueAsString(listSalida);
			return Response.ok(json).build();
		} catch (DomainNotExistsException | UserNotExistsException
				| MessageNotFoundException | IOException e) {
			return Response.serverError().entity(e.getMessage()).build();
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{idDomain}/{idUser}/{idMessage}")
	public Response readMessage(@PathParam("idDomain") String idDomain,
			@PathParam("idUser") String idUser,
			@PathParam("idMessage") String idMessage) {

		Domain domain = buildDomainData(idDomain, idUser, idMessage);
		try {
			Domain messageForUser = messageService.getMessageForUser(domain);
			return Response.ok(messageForUser).build();
		} catch (MessageNotFoundException | DomainNotExistsException
				| UserNotExistsException e) {
			return Response.serverError().entity(e.getMessage()).build();
		}

	}

	public Domain buildDomainData(String idDomain, String idUser,
			String idMessage) {
		Domain domain = new Domain.Builder().idGenerado(idDomain).build();
		User user = new User.Builder().idGenerado(idUser).build();
		user.addMessage(new Message.Build().idGenerado(idMessage).build());
		domain.addUserToDomain(user);
		return domain;
	}

	@DELETE
	@Path("/{idDomain}/{idUser}/{idMessage}")
	public Response deleteMessage(@PathParam("idDomain") String idDomain,
			@PathParam("idUser") String idUser,
			@PathParam("idMessage") String idMessage) {

		Domain domain = buildDomainData(idDomain, idUser, idMessage);
		try {
			messageService.removeMessage(domain);
		} catch (DomainNotExistsException | UserNotExistsException e) {
			return Response.serverError().entity(e.getMessage()).build();
		}
		return Response.ok().build();

	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createMessage(MessageInput messageInput) {

		Domain domain = new Domain.Builder().idGenerado(
				messageInput.getIdDomain()).build();
		User user = new User.Builder().idGenerado(messageInput.getIdUser())
				.build();
		Message message = new Message.Build()
				.content(messageInput.getContent())
				.subject(messageInput.getSubject())
				.cc(Arrays.asList(messageInput.getDestinationsInCopy()))
				.cco(Arrays.asList(messageInput.getDestinationsHiddens()))
				.to(Arrays.asList(messageInput.getDestinations()))
				.from(messageInput.getUserName()).build();
		user.addMessage(message);
		domain.addUserToDomain(user);
		try {
			messageService.addMessageInDomain(domain);
			return Response.ok().build();
		} catch (DomainNotExistsException | UserNotExistsException e) {
			return Response.serverError().entity(e.getMessage()).build();
		}

	}
}
