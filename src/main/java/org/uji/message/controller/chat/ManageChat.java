package org.uji.message.controller.chat;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.websocket.EncodeException;
import javax.websocket.EndpointConfig;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.codehaus.jackson.map.ObjectMapper;
import org.uji.message.dto.Chat;
import org.uji.message.exception.ChatNotInitException;
import org.uji.message.input.chat.ChatInput;
import org.uji.message.services.ChatService;

/**
 * Websocket expuesto para el chat entre distintos contactos de un dominio Esta
 * clase se apoya en un mapa sincronizado y estatico para que sea posible el
 * macheo de sesiones y realizar los envios correctamente
 * 
 * @author angel
 * */
@ServerEndpoint("/chat")
public class ManageChat {

	private static final String ERR_NO_UNIQ_CHAT = "no puede iniciar el chat con la misma persona sin cerrar el anterior";
	private static final String ERR_NO_CONECTADO = "no estas conectado";
	private static final String ERR_NO_INIT_CHAT = "no se ha podido inciar el chat";
	private static final String ERR_NO_EXISTE_DESTINO = "no existe el chat destino";
	public static Map<String, Session> mapSessionsConected = Collections
			.synchronizedMap(new HashMap<String, Session>());

	@Inject
	private ChatService chatService;

	@OnMessage
	public void sendMessage(String message, Session client) throws IOException,
			EncodeException {
		ObjectMapper mapper = new ObjectMapper();
		ChatInput mensaje = mapper.readValue(message, ChatInput.class);
		String keyConnected = getKey(mensaje.getUserName(),
				mensaje.getDomainName(), mensaje.getUserNameDest());
		synchronized (mapSessionsConected) {
			for (String string : mapSessionsConected.keySet()) {
				System.out.println(" sessions " + string);
			}
			if (mapSessionsConected.containsKey(keyConnected)) {
				String keyDest = getKey(mensaje.getUserNameDest(),
						mensaje.getDomainName(), mensaje.getUserName());
				if (mapSessionsConected.containsKey(keyDest)) {
					Session clientDest = mapSessionsConected.get(keyDest);
					clientDest.getBasicRemote().sendText(
							mensaje.getMessageContent());
				} else {
					sendErrorMessage(client, ERR_NO_EXISTE_DESTINO);
				}
			} else {
				sendErrorMessage(client, ERR_NO_CONECTADO);

			}
		}

	}

	@OnOpen
	public void openChat(Session session, EndpointConfig conf)
			throws IOException {

		String[] parms = session.getQueryString().split("&");
		Map<String, String> pathParameters = new HashMap<>();
		for (String param : parms) {
			String[] data = param.split("=");
			pathParameters.put(data[0], data[1]);
		}

		String userName = pathParameters.get("userName");
		System.out.println(userName + " afdd");
		String domainName = pathParameters.get("domainName");
		System.out.println(domainName + " afdd");
		String userNameDest = pathParameters.get("userDest");
		System.out.println(userNameDest + " afdd");

		String key = getKey(userName, domainName, userNameDest);
		if (mapSessionsConected.containsKey(key)) {
			sendErrorMessage(session, ERR_NO_UNIQ_CHAT);
		} else {
			mapSessionsConected.put(key, session);
		}
		initChatInBD(session, userName, domainName, userNameDest);

	}

	private void initChatInBD(Session session, String userName,
			String domainName, String userNameDest) throws IOException {

		Chat chat = new Chat();
		chat.setDomainName(domainName);
		chat.setUserName(userName);
		chat.setUserNameDest(userNameDest);
		try {
			chatService.initChat(chat);
		} catch (ChatNotInitException e) {
			System.err.println(e.toString());
			sendErrorMessage(session, ERR_NO_INIT_CHAT);
		}
	}

	private void sendErrorMessage(Session session, String msg)
			throws IOException {
		session.getBasicRemote().sendText(msg);
	}

	private String getKey(String userName, String domainName,
			String userNameDest) {
		StringBuffer sb = new StringBuffer();
		sb.append(userName).append(":").append(domainName).append(":")
				.append(userNameDest);
		return sb.toString();
	}

}
