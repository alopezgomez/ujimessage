package org.uji.message.controller;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.uji.message.dto.Domain;
import org.uji.message.dto.User;
import org.uji.message.exception.ContacNotExistsException;
import org.uji.message.exception.DomainNotExistsException;
import org.uji.message.services.ContactService;

/**
 * Servicio REST que controla las operaciones sobre los contactos del dominio
 * 
 * @author angel
 * */
@Path("/contact")
public class ManageContacts {

	@Inject
	private ContactService contactService;

	@GET
	@Path("/{idDomain}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getAllContacts(@PathParam("idDomain") String idDomain) {

		try {
			Domain domain = new Domain.Builder().idGenerado(idDomain).build();
			List<User> allContacts = contactService.getAllContacts(domain);
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Inclusion.NON_NULL);
			String json = mapper.writeValueAsString(allContacts);
			return Response.ok(json).build();

		} catch (DomainNotExistsException | IOException e) {
			return Response.serverError().entity(e.getMessage()).build();
		}

	}

	@GET
	@Path("/{idDomain}/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getContact(@PathParam("idDomain") String idDomain,
			@PathParam("id") String idContact) {

		try {
			Domain domain = new Domain.Builder().idGenerado(idDomain).build();
			domain.addUserToDomain(new User.Builder().idGenerado(idContact)
					.build());
			User contact = contactService.getContact(domain);
			return Response.ok(contact).build();
		} catch (ContacNotExistsException | DomainNotExistsException e) {
			return Response.serverError().entity(e.getMessage()).build();
		}
	}
}