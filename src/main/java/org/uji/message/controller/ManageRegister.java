package org.uji.message.controller;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.uji.message.dto.Domain;
import org.uji.message.dto.User;
import org.uji.message.exception.DomainAlreadyRegisteredException;
import org.uji.message.input.DomainInput;
import org.uji.message.services.RegisterService;

/**
 * Servicio REST que controla las operaciones de registro sobre el dominio
 * 
 * 
 * @author angel
 * */
@Path("/register")
public class ManageRegister {

	@Inject
	private @Named("registerService")
	RegisterService registerService;

	@POST
	@Path("/domain")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response putDomain(DomainInput domainInput) {

		Domain domain = new Domain();
		User userAdmin = new User();

		domain.setNomDomain(domainInput.getDomainName());
		userAdmin.setUserName(domainInput.getUserName());
		userAdmin.setPassword(domainInput.getPassword());
		domain.setUserAdmin(userAdmin);

		try {
			registerService.registerDomain(domain);
		} catch (DomainAlreadyRegisteredException e) {
			return Response.serverError().entity(e.getMessage()).build();
		}

		return Response.ok(domain.getIdGenerado()).build();
	}

}
