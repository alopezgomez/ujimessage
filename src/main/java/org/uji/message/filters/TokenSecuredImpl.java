package org.uji.message.filters;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import org.uji.message.annotations.security.TokenSecured;
import org.uji.message.security.Authenticator;
import org.uji.message.security.Token;
import org.uji.message.security.TypeSecurity;
import org.uji.message.utils.Constantes;
import org.uji.message.utils.SecurityUtils;

@Provider
@TokenSecured
public class TokenSecuredImpl implements ContainerRequestFilter {

	public TokenSecuredImpl() {
	}

	@Override
	public void filter(ContainerRequestContext requestContext)
			throws IOException {
		System.out.println(requestContext.getUriInfo().getPath());

		if (!Constantes.PATHS_WITHOT_TOKEN.contains(requestContext.getUriInfo()
				.getPath())) {

			String tokenHeader = requestContext
					.getHeaderString(Constantes.TOKEN);
			String createdData = requestContext
					.getHeaderString(Constantes.CREATED_DATA);
			String userAllow = requestContext
					.getHeaderString(Constantes.USER_ALLOW);

			String domain = userAllow.split(":")[1];
			String userName = userAllow.split(":")[0];

			String[] parmsToken = { domain, userName, createdData };

			try {
				Token token = SecurityUtils.getSecurityHashed(
						TypeSecurity.SESSION_TOKEN, parmsToken);

				if (token.getToken().equals(tokenHeader)) {
					calculateExpirationToken(requestContext, createdData);
				} else {
					requestContext.abortWith(Response.status(
							Status.UNAUTHORIZED).build());
				}

				requestContext.setSecurityContext(new Authenticator(userName,
						domain));

			} catch (NoSuchAlgorithmException e) {
				requestContext.abortWith(Response.serverError().build());
			}

		}

	}

	private void calculateExpirationToken(
			ContainerRequestContext requestContext, String createdData) {
		Long dateCreated = Long.parseLong(createdData);
		Calendar calendar = Calendar.getInstance();
		Long dateNow = calendar.getTimeInMillis();
		Long diff = dateNow - dateCreated;
		Long diffInMinuter = diff / (1000 * 60);
		if (diffInMinuter.intValue() > Constantes.TOKEN_EXPIRATION_DEFAULT
				.intValue()) {
			requestContext.abortWith(Response.status(Status.EXPECTATION_FAILED)
					.entity(Constantes.MSG_ERR_NEW_TOKEN).build());
		}
	}

}
