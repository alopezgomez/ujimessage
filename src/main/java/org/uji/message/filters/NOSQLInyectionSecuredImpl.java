package org.uji.message.filters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

import org.uji.message.annotations.security.NOSQLInyectionSecured;

@Provider
@NOSQLInyectionSecured
public class NOSQLInyectionSecuredImpl implements ContainerRequestFilter {

	public NOSQLInyectionSecuredImpl() {
	}

	@Override
	public void filter(ContainerRequestContext requestContext)
			throws IOException {

		if (requestContext.hasEntity()) {

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					requestContext.getEntityStream()));
			String linea = "";
			while ((linea = reader.readLine()) != null) {
				// TODO checkear con los ejecutables js de mongodb
			}

		}

	}

}
