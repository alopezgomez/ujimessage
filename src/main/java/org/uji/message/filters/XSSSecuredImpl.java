package org.uji.message.filters;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

import org.uji.message.annotations.security.XSSSecured;

@Provider
@XSSSecured
public class XSSSecuredImpl implements ContainerRequestFilter {

	public XSSSecuredImpl() {
	}

	@Override
	public void filter(ContainerRequestContext paramContainerRequestContext)
			throws IOException {
		// TODO Auto-generated method stub

	}

}
