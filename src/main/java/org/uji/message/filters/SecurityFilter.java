package org.uji.message.filters;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.regex.Pattern;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import org.uji.message.annotations.security.Secured;
import org.uji.message.security.Authenticator;
import org.uji.message.security.Token;
import org.uji.message.security.TypeSecurity;
import org.uji.message.utils.Constantes;
import org.uji.message.utils.SecurityUtils;

@Provider
@Secured
public class SecurityFilter implements ContainerRequestFilter {

	public SecurityFilter() {
	}

	@Override
	public void filter(ContainerRequestContext requestContext)
			throws IOException {

		if (!Constantes.PATHS_WITHOT_TOKEN.contains(requestContext.getUriInfo()
				.getPath())) {

			String eval = convertInputStreamToString(requestContext
					.getEntityStream());

			String evaluated = checkForXSS(eval);
			if (!eval.equals(evaluated)) {
				requestContext.abortWith(Response.serverError()
						.entity("XSS Detectado").build());
			}

			String tokenHeader = requestContext
					.getHeaderString(Constantes.TOKEN);
			String createdData = requestContext
					.getHeaderString(Constantes.CREATED_DATA);
			String userAllow = requestContext
					.getHeaderString(Constantes.USER_ALLOW);

			String domain = userAllow.split(":")[1];
			String userName = userAllow.split(":")[0];

			String[] parmsToken = { domain, userName, createdData };

			try {
				Token token = SecurityUtils.getSecurityHashed(
						TypeSecurity.SESSION_TOKEN, parmsToken);

				if (token.getToken().equals(tokenHeader)) {
					calculateExpirationToken(requestContext, createdData);
				} else {
					requestContext.abortWith(Response.status(
							Status.UNAUTHORIZED).build());
				}

				requestContext.setSecurityContext(new Authenticator(userName,
						domain));
				InputStream is = new ByteArrayInputStream(eval.getBytes());
				requestContext.setEntityStream(is);
			} catch (NoSuchAlgorithmException e) {
				requestContext.abortWith(Response.serverError().build());
			}

		}

	}

	private void calculateExpirationToken(
			ContainerRequestContext requestContext, String createdData) {
		Long dateCreated = Long.parseLong(createdData);
		Calendar calendar = Calendar.getInstance();
		Long dateNow = calendar.getTimeInMillis();
		Long diff = dateNow - dateCreated;
		Long diffInMinuter = diff / (1000 * 60);
		if (diffInMinuter.intValue() > Constantes.TOKEN_EXPIRATION_DEFAULT
				.intValue()) {
			requestContext.abortWith(Response.status(Status.EXPECTATION_FAILED)
					.entity(Constantes.MSG_ERR_NEW_TOKEN).build());
		}
	}

	private String checkForXSS(String eval) {
		if (eval != null) {

			String value = eval;

			value = value.replaceAll("", "");

			Pattern scriptPattern = Pattern.compile("<script>(.*?)</script>",
					Pattern.CASE_INSENSITIVE);
			value = scriptPattern.matcher(value).replaceAll("");

			scriptPattern = Pattern.compile("src[\r\n]*=[\r\n]*\\\'(.*?)\\\'",
					Pattern.CASE_INSENSITIVE | Pattern.MULTILINE
							| Pattern.DOTALL);
			value = scriptPattern.matcher(value).replaceAll("");

			scriptPattern = Pattern.compile("src[\r\n]*=[\r\n]*\\\"(.*?)\\\"",
					Pattern.CASE_INSENSITIVE | Pattern.MULTILINE
							| Pattern.DOTALL);
			value = scriptPattern.matcher(value).replaceAll("");

			scriptPattern = Pattern.compile("</script>",
					Pattern.CASE_INSENSITIVE);
			value = scriptPattern.matcher(value).replaceAll("");

			scriptPattern = Pattern.compile("<script(.*?)>",
					Pattern.CASE_INSENSITIVE | Pattern.MULTILINE
							| Pattern.DOTALL);
			value = scriptPattern.matcher(value).replaceAll("");

			scriptPattern = Pattern.compile("eval\\((.*?)\\)",
					Pattern.CASE_INSENSITIVE | Pattern.MULTILINE
							| Pattern.DOTALL);
			value = scriptPattern.matcher(value).replaceAll("");
			return value;
		}
		return eval;

	}

	private String convertInputStreamToString(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();

	}

}
