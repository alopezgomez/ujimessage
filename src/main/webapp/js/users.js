function Users(){
	
	var dataHeader = function(){		
		var tokenSession = JSON.parse(sessionStorage.getItem("auth"));
		return ["token",tokenSession.token,"createdData",tokenSession.createdData,"userAllow",tokenSession.userAllow];
	};
	
	var dataSession = JSON.parse(sessionStorage.getItem("data"));
	
	this.getAllUsers = function(grid){				
		var url = '/message-api/api/admin/'+dataSession.idDomain+'/user';
		 new Ajax.Request(url,{			 	
			 	method:'get',
			 	contentType : "application/json",
			 	requestHeaders:dataHeader(),
	  		 	onComplete: function(response){
	  		 		if (200==response.status){  		 		
	  		 			var store = grid.getStore();
	  		 			store.removeAll();
	  		 			store.loadData(response.responseJSON);
	  		 		}else if(403==response.status){
	  		 			alert("No puede utilizar la opcion de administración, gracias");
	  		 		}else if(401==response.status){
	  		 			window.location.href = 'index.html';
	  		 		}else{
	  		 			alert(response.responseText);
	  		 		}
	  		 	}
	  		 });
	};
	
	this.removeUser = function(grid,row){		
		var url = '/message-api/api/admin/'+dataSession.idDomain+'/user/'+row.json.idUser;
		 new Ajax.Request(url,{			 	
			 	method:'delete',
			 	contentType : "application/json",
			 	requestHeaders:dataHeader(),
	  		 	onComplete: function(response){
	  		 		if (200==response.status){  		 		
	  		 			getAllMessages(grid);
	  		 		}else if(401==response.status){
	  		 			window.location.href = 'index.html';
	  		 		}else{
	  		 			alert(response.responseText);
	  		 		}
	  		 	}
	  		 });
	};
	
	this.putUser = function(valores){
		var objMessage={
          		"userName":valores.userName,
          		"password":valores.password,          		
          		"idDomain": dataSession.idDomain,
          		"idUser":valores.idUser          		
          };
		  new Ajax.Request('/message-api/api/admin/'+dataSession.idDomain+'/user',{
  		 	method:'post',
  		 	parameters:Object.toJSON(objMessage),
  		 	contentType : "application/json",
  		 	requestHeaders:dataHeader(),
  		 	onComplete: function(response){
  		 		if (200!=response.status){  		 		
  		 			alert(response.responseText);
  		 		}else if(401==response.status){
  		 			window.location.href = 'index.html';
  		 		}
  		 	}
  		 });
	}	
	
}