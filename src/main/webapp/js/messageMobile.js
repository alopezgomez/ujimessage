function Messages(){	
	
	var tokenSession = JSON.parse(sessionStorage.getItem("auth"));
	var dataSession = JSON.parse(sessionStorage.getItem("data"));
	
	this.getAllMessages = function(){				
		$.ajax({
	        beforeSend: function(xhrObj){
	                xhrObj.setRequestHeader("Content-Type","application/json");
	                xhrObj.setRequestHeader("Accept","application/json");
	                xhrObj.setRequestHeader("token",tokenSession.token);
	                xhrObj.setRequestHeader("createdData",tokenSession.createdData);
	                xhrObj.setRequestHeader("userAllow",tokenSession.userAllow);
	        },
	        type: "get",
	        url: '/message-api/api/message/'+dataSession.idDomain+'/'+dataSession.idUser,
	        processData: false,
	        dataType: "json",
	        success: function(json){
	            $("#listMensajes li").remove();
	        	$.each(json,function(i,item){	        	
		            $("<a/>",{
		            	href : "#formContainer",
		            	text : 'De '+item.destinationsStr+"; Asunto:"+item.subject,
		            	click :function(){
		            		$('#formContainer').show();
		            		$('#bandeja').hide();
		            		$("#para").val(item.destinationsStr);
		            		$("#asunto").val("RE "+item.subject);
		            		$("#contenido").val(item.content);		
		            	}
		            }).appendTo($("<li/>").appendTo(("#listMensajes")));   
		        });	        	
	        },
	        error: function(e, xhr, settings){
  		 		if(401==e.status){
  		 			window.location.href = 'index.html';
  		 		}else if (200!=e.status){  		 		
  		 			alert("Error al recibir el mensaje");
  		 		}
	        }
		});		
	};
	
	
	this.postMessage = function(){
		var objMessage={
          		"destinations":$("#para").val().split(","),
          		"destinationsInCopy":$("#copia").val().split(","),
          		"destinationsHiddens":$("#oculta").val().split(","),
          		"subject":$("#asunto").val(),
          		"content":$("#contenido").val(),
          		"idDomain": dataSession.idDomain,
          		"idUser":dataSession.idUser,
          		"userName":dataSession.userName
          };
		$.ajax({
	        beforeSend: function(xhrObj){
	                xhrObj.setRequestHeader("Content-Type","application/json");
	                xhrObj.setRequestHeader("Accept","application/json");
	                xhrObj.setRequestHeader("token",tokenSession.token);
	                xhrObj.setRequestHeader("createdData",tokenSession.createdData);
	                xhrObj.setRequestHeader("userAllow",tokenSession.userAllow);
	        },
	        type: "POST",
	        url: "/message-api/api/message",
	        processData: false,
	        data: JSON.stringify(objMessage),
	        dataType: "json",
	        error: function(e, xhr, settings){
  		 		if(401==e.status){
  		 			window.location.href = 'index.html';
  		 		}else if (200!=e.status){  		 		
  		 			alert("Error al enviar el mensaje");
  		 		}
	        }
		});	
	
	}	
	
}