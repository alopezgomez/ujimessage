function Chat(){
	
	var dataSession = JSON.parse(sessionStorage.getItem("data"));
	
	var that = this;
	
	var queryParms = '?userName='+dataSession.userName+"&domainName="+dataSession.domainName+"&userDest="+Ext.getCmp("userDest").getValue();
	
	var ws= new WebSocket("ws://"+window.location.host+"/message-api/chat"+queryParms);
	  
	  var putDataMessage = function(content){
    	  var valueChat = Ext.getCmp("content").getValue()+'\n'+content;
    	  Ext.getCmp("content").setValue(valueChat);
    	  return;
      };
      
	 
      ws.onopen = function(event) {
    	  putDataMessage("Esperando a "+Ext.getCmp("userDest").getValue());   
      };
      
      ws.onmessage = function(event) {
    	  putDataMessage("Response: "+event.data);      
      };
      
      ws.onclose = function(event) {};

      ws.onerror = function(event){
              alert("Error :  " + event.data);
      };
      
      this.closeChat = function(data){
    	  ws.close();
    	  Ext.getCmp("content").setValue('');
    	  Ext.getCmp("userDest").setValue('');
    	  Ext.getCmp("message").setValue('');
    	  
      }
      
      this.sendMessage = function(){
    	  var message = Ext.getCmp("message").getValue();
    	  var objMessage={
  	      		"userName":dataSession.userName,
  	      		"domainName":dataSession.domainName,
  	      		"userNameDest":Ext.getCmp("userDest").getValue(),
  	      		"messageContent": message       		
  	      };
    	  Ext.getCmp("message").setValue('');
    	  ws.send(JSON.stringify(objMessage));
    	  putDataMessage("Send: "+message);
    	  
      };
	
}