function Messages(){
	
	var dataHeader = function(){		
		var tokenSession = JSON.parse(sessionStorage.getItem("auth"));
		return ["token",tokenSession.token,"createdData",tokenSession.createdData,"userAllow",tokenSession.userAllow];
	};
	
	var dataSession = JSON.parse(sessionStorage.getItem("data"));
	
	this.getAllMessages = function(grid){				
		var url = '/message-api/api/message/'+dataSession.idDomain+'/'+dataSession.idUser;
		 new Ajax.Request(url,{			 	
			 	method:'get',
			 	contentType : "application/json",
			 	requestHeaders:dataHeader(),
	  		 	onComplete: function(response){
	  		 		if (200==response.status){  		 		
	  		 			var store = grid.getStore();
	  		 			store.removeAll();
	  		 			store.loadData(response.responseJSON);
	  		 		}else if(401==response.status){
	  		 			window.location.href = 'index.html';
	  		 		}else{
	  		 			alert(response.responseText);
	  		 		}
	  		 	}
	  		 });
	};
	
	this.removeMessage = function(grid,row){		
		var url = '/message-api/api/message/'+dataSession.idDomain+'/'+dataSession.idUser+'/'+row.idMessage;
		 new Ajax.Request(url,{			 	
			 	method:'delete',
			 	contentType : "application/json",
			 	requestHeaders:dataHeader(),
	  		 	onComplete: function(response){
	  		 		if (200==response.status){  		 		
	  		 			getAllMessages(grid);
	  		 		}else if(401==response.status){
	  		 			window.location.href = 'index.html';
	  		 		}else{
	  		 			alert(response.responseText);
	  		 		}
	  		 	}
	  		 });
	};
	
	this.postMessage = function(valores){
		var objMessage={
          		"destinations":valores.para.split(","),
          		"destinationsInCopy":valores.copia.split(","),
          		"destinationsHiddens":valores.oculta.split(","),
          		"subject":valores.asunto,
          		"content":valores.contenido,
          		"idDomain": dataSession.idDomain,
          		"idUser":dataSession.idUser,
          		"userName":dataSession.userName
          };
		  new Ajax.Request('/message-api/api/message',{
  		 	method:'post',
  		 	parameters:Object.toJSON(objMessage),
  		 	contentType : "application/json",
  		 	requestHeaders:dataHeader(),
  		 	onComplete: function(response){
  		 		if (200!=response.status){  		 		
  		 			alert(response.responseText);
  		 		}else if(401==response.status){
  		 			window.location.href = 'index.html';
  		 		}
  		 	}
  		 });
	}	
	
}