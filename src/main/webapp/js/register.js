function Register() {
	
	var _json;
	var _userLogin = $F('userLogin');
	var _userPass = $F('userPass');
	
	this.login = function(){
		  
		  var _userName = _userLogin.split("@")[0];
		  var _domainName = _userLogin.split("@")[1];
		  _json = {
				  "userName" : _userName,
				  "domainName" : _domainName,
				  "password" : _userPass
		  }; 
			 

		  new Ajax.Request('/message-api/api/auth/token',{
		 	method:'post',
		 	parameters:Object.toJSON(_json),
		 	contentType : "application/json",
		 	onComplete: function(response){
		 		if (200==response.status){
		 			var jsonRespuesta = response.responseJSON;
		 			var token = response.getResponseHeader("token");
		 			var createdData = response.getResponseHeader("createdData");
		 			var userAllow = response.getResponseHeader("userAllow");
		 			var _autToken = {"token":token,"createdData":createdData,"userAllow":userAllow};
		 			sessionStorage.setItem("auth",JSON.stringify(_autToken));
		 			sessionStorage.setItem("data",JSON.stringify(jsonRespuesta));
		 			var dispositivo = navigator.userAgent.toLowerCase();
		 		      if(dispositivo.search(/iphone|ipod|ipad|android/) > -1 ){
		 		    	 window.location.href="uiMessageMobile.html"; 
		 		     }else{		 		    	
			 			window.location.href="uiMessage.html"; 
		 		     }		 			
		 		}else{
		 			alert(response.responseText);
		 		}
		 	}
		 });
	};
	
	this.createDomain = function(){
		 _json = $('registerDomain').serialize(true);
		 var that = this;
		 new Ajax.Request('/message-api/api/register/domain',{
		 	method:'post',
		 	contentType : "application/json",
		 	parameters:Object.toJSON(_json),
		 	onComplete: function(response){
		 		if (200==response.status){
		 			_userLogin = _json.userName+"@"+_json.domainName;
		 			_userPass = _json.password;
		 			that.login();
		 		}else{
		 			alert(response.responseText);
		 		}
		 	}
		 });
	};
	


}