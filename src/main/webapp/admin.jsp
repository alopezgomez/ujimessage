<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
   
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Estadiscas de dominios</title>
<style type="text/css">

#hor-minimalist-a
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 12px;
	background: #fff;
	margin: 45px;
	width: 480px;
	border-collapse: collapse;
	text-align: left;
}
#hor-minimalist-a th
{
	font-size: 14px;
	font-weight: normal;
	color: #039;
	padding: 10px 8px;
	border-bottom: 2px solid #6678b1;
}
#hor-minimalist-a td
{
	color: #669;
	padding: 9px 8px 0px 8px;
}
#hor-minimalist-a tbody tr:hover td
{
	color: #009;
}


#hor-minimalist-b
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 12px;
	background: #fff;
	margin: 45px;
	width: 480px;
	border-collapse: collapse;
	text-align: left;
}
#hor-minimalist-b th
{
	font-size: 14px;
	font-weight: normal;
	color: #039;
	padding: 10px 8px;
	border-bottom: 2px solid #6678b1;
}
#hor-minimalist-b td
{
	border-bottom: 1px solid #ccc;
	color: #669;
	padding: 6px 8px;
}
#hor-minimalist-b tbody tr:hover td
{
	color: #009;
}


</style>
<link rel="stylesheet" type="text/css" href="css/jquery.jqplot.css" />

<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.jqplot.min.js"></script>
<script class="include" type="text/javascript" src="js/plugins/jqplot.pieRenderer.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$.jqplot.config.enablePlugins = true;
	var urlUserDomain = '/message-api/domain';
	  var urlMensajeDominio = '/message-api/message';
	  var urlMensajeUsuario = '/message-api/user';

	 $.getJSON(urlUserDomain, "", function (data) {
	        var dataSlices = [];
	        var dataLabels = "";

	        $.each(data, function (entryindex, entry) {
	            dataSlices.push([entry['name'],entry['value']]);
	            dataLabels = dataLabels + entry['name'];
	        });
	        options = {
	            legend: { show: true },
	            title: 'Usuarios en Dominios',
	            seriesDefaults: {
	                renderer: $.jqplot.PieRenderer,
	                rendererOptions: {
	                    showDataLabels: true
	                }
	            }
	        }
	        var plotUserDomain = $.jqplot('usariosDominio', [dataSlices], options);
	    });
	
	 $.getJSON(urlMensajeDominio, "", function (data) {
	        var dataSlices = [];
	        var dataLabels = "";

	        $.each(data, function (entryindex, entry) {
	            dataSlices.push([entry['name'],entry['value']]);
	            dataLabels = dataLabels + entry['name'];
	        });
	        options = {
	            legend: { show: true },
	            title: 'Mensajes por Dominios',
	            seriesDefaults: {
	                renderer: $.jqplot.PieRenderer,
	                rendererOptions: {
	                    showDataLabels: true
	                }
	            }
	        }
	        var plotMensajeUser = $.jqplot('mensajesUusario', [dataSlices], options);
	    });
	
	 $.getJSON(urlMensajeUsuario, "", function (data) {
	        var dataSlices = [];
	        var dataLabels = "";

	        $.each(data, function (entryindex, entry) {
	            dataSlices.push([entry['name'],entry['value']]);
	            dataLabels = dataLabels + entry['name'];
	        });
	        options = {
	            legend: { show: true },
	            title: 'Mensajes por Usuarios',
	            seriesDefaults: {
	                renderer: $.jqplot.PieRenderer,
	                rendererOptions: {
	                    showDataLabels: true
	                }
	            }
	        }
	        var plotMensajeDominio = $.jqplot('mensajeDominio', [dataSlices], options);
	    });	
	
	});
</script>
</head>
<body>
<header><h1>Estadisticas Aplicacion</h1></header>
	<article>
		<section>
			<table id="hor-minimalist-a">
				<thead>
					<tr>
						<th scope="col">Nombre Dominio</th>
						<th scope="col">Numero Ususarios</th>
						<th scope="col">Numero Mensajes</th>
					</tr>
				</thead>
				<tbody>
			    <c:forEach var="data" items="${listData}"> 
		            <tr>
		            	<td> <c:out value="${data.nomDomin}"/> </td>
		            	<td> <c:out value="${data.numUsers}"/> </td>
		            	<td> <c:out value="${data.numMessages}"/> </td>
		            </tr>
		            
		        </c:forEach>
		        </tbody>			
			</table>
		</section>
	</article>
	<aside>
		<div id="graficas">
			<div id="usariosDominio" style="float: left;"></div>
			<div id="mensajesUusario" style="float: left;"></div>
			<div id="mensajeDominio" style="float: left;"></div>
		</div>
	</aside>

</body>
</html>