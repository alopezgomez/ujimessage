package org.uji.message.services;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.uji.message.dto.Domain;
import org.uji.message.dto.User;
import org.uji.message.exception.DomainAlreadyRegisteredException;
import org.uji.message.exception.DomainNotExistsException;
import org.uji.message.security.Token;
import org.uji.message.security.TypeSecurity;
import org.uji.message.services.impl.RegisterServiceImpl;
import org.uji.message.utils.SecurityUtils;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

public class TestServiceRegister {

	private static final String DOMAIN_FOR_TEST = "pepe";
	private RegisterService service;
	private DBCollection collection;

	private Domain prepareData() {
		Domain domain = new Domain();
		domain.setNomDomain(DOMAIN_FOR_TEST);
		domain.setUserAdmin(createUserAdminForDomain());
		return domain;
	}

	private String findPassworUserAdmin(Domain domain)
			throws JsonGenerationException, JsonMappingException, IOException {
		BasicDBObject basic = (BasicDBObject) JSON.parse(domain.toJSON());
		DBCursor find = collection.find(basic);
		DBObject objectFind = find.next();

		DBObject userAdmin = (DBObject) objectFind.get("userAdmin");

		String passCreated = (String) userAdmin.get("password");
		return passCreated;
	}

	private Domain insertDomain() throws DomainAlreadyRegisteredException {
		Domain domain = prepareData();
		service.registerDomain(domain);
		return domain;
	}

	private User createUserForDomain() throws NoSuchAlgorithmException {
		User user = new User();
		user.setUserName(DOMAIN_FOR_TEST);
		Token tokenForUser = SecurityUtils.getSecurityHashed(
				TypeSecurity.PASSWORD, new String[] { DOMAIN_FOR_TEST,
						"asdasdas" });
		user.setPassword(tokenForUser.getToken());
		return user;
	}

	private User createUserAdminForDomain() {
		User user = new User();
		user.setUserName("adsdas");
		user.setPassword("asdas");
		return user;
	}

	@Before
	public void setUp() throws UnknownHostException {
		service = new RegisterServiceImpl();
		MongoClient client = new MongoClient();
		DB db = client.getDB("ujiMessage");
		collection = db.getCollection("domain");

	}

	@After
	public void tearDown() throws UnknownHostException {

		DBCursor dbCursor = collection.find();
		while (dbCursor.hasNext()) {
			DBObject dbObject = dbCursor.next();
			collection.remove(dbObject);
		}
	}

	@Test
	public void register_domain_user_no_exists()
			throws DomainAlreadyRegisteredException {
		Domain domain = prepareData();
		service.registerDomain(domain);
	}

	@Test(expected = DomainAlreadyRegisteredException.class)
	public void can_not_register_domain_is_domain_exists()
			throws DomainAlreadyRegisteredException {

		Domain domain = insertDomain();
		service.registerDomain(domain);

	}

	@Test(expected = DomainNotExistsException.class)
	public void throw_exception_when_domain_not_exists_for_user()
			throws NoSuchAlgorithmException, DomainNotExistsException {

		Domain domain = prepareData();
		User user = createUserForDomain();
		domain.addUserToDomain(user);
		service.existsDomain(domain);

	}

	@Test
	public void register_user_admin_domain_hash_password() throws Exception {

		Domain domain = prepareData();
		Token tokenExpected = SecurityUtils.getSecurityHashed(
				TypeSecurity.PASSWORD, new String[] {
						domain.getUserAdmin().getUserName(),
						domain.getUserAdmin().getPassword() });

		service.registerDomain(domain);
		String passCreated = findPassworUserAdmin(domain);

		assertEquals(tokenExpected.getToken(), passCreated);
	}

}
