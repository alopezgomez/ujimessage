package org.uji.message.services;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.uji.message.dto.Domain;
import org.uji.message.dto.Message;
import org.uji.message.dto.User;
import org.uji.message.exception.DomainNotExistsException;
import org.uji.message.exception.MessageNotFoundException;
import org.uji.message.exception.UserNotExistsException;
import org.uji.message.services.impl.MessageServiceImpl;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

public class TestMessageService {

	private DBCollection collection;
	private MessageServiceImpl service;

	@Before
	public void setUp() throws IOException {
		MongoClient client = new MongoClient();
		DB db = client.getDB("ujiMessage");
		collection = db.getCollection("domain");
		BasicDBObject basic = (BasicDBObject) JSON.parse(setDataDomain()
				.toJSON());
		collection.insert(basic);
		service = new MessageServiceImpl();

	}

	@After
	public void tearDown() throws IOException {
		Domain domainDelete = new Domain.Builder().nomDomain("domain").build();
		DBCursor find = collection.find((DBObject) JSON.parse(domainDelete
				.toJSON()));
		while (find.hasNext()) {
			collection.remove(find.next());
		}
	}

	@Test
	public void obtain_all_message_for_user_in_domain()
			throws DomainNotExistsException, UserNotExistsException,
			MessageNotFoundException {

		Domain domain = setDataDomain();

		List<Domain> listResults = service.getAllMessages(domain);
		assertThat(listResults.size(), is(1));

	}

	@Test(expected = DomainNotExistsException.class)
	public void error_when_obtain_all_messages_for_domain_not_exists()
			throws DomainNotExistsException, UserNotExistsException,
			MessageNotFoundException {

		Domain domain = new Domain();
		service.getAllMessages(domain);
	}

	@Test(expected = UserNotExistsException.class)
	public void error_when_obtain_all_messages_for_user_not_exists()
			throws DomainNotExistsException, UserNotExistsException,
			MessageNotFoundException {

		Domain domain = new Domain.Builder().nomDomain("domain")
				.idGenerado("1").build();
		domain.addUserToDomain(new User());

		service.getAllMessages(domain);

	}

	@Test
	public void obtain_a_message_for_user_in_domain()
			throws MessageNotFoundException, DomainNotExistsException,
			UserNotExistsException {

		Message message = new Message.Build().idGenerado("4").build();
		User user = new User.Builder().idGenerado("1").build();
		user.addMessage(message);
		Domain domain = new Domain.Builder().nomDomain("domain")
				.idGenerado("1").build();
		domain.addUserToDomain(user);

		assertNotNull(service.getMessageForUser(domain));

	}

	@Test(expected = UserNotExistsException.class)
	public void obtain_a_message_for_user_no_exists_in_domain()
			throws MessageNotFoundException, DomainNotExistsException,
			UserNotExistsException {

		Message message = new Message.Build().idGenerado("4").build();
		User user = new User.Builder().idGenerado("5").build();
		user.addMessage(message);
		Domain domain = new Domain.Builder().idGenerado("1").build();
		domain.addUserToDomain(user);

		service.getMessageForUser(domain);

	}

	@Test(expected = DomainNotExistsException.class)
	public void obtain_a_message_for_user_in_domain_no_exists()
			throws MessageNotFoundException, DomainNotExistsException,
			UserNotExistsException {

		Message message = new Message.Build().idGenerado("4").build();
		User user = new User.Builder().idGenerado("1").build();
		user.addMessage(message);
		Domain domain = new Domain.Builder().idGenerado("3").build();
		domain.addUserToDomain(user);

		service.getMessageForUser(domain);

	}

	@Test
	public void put_new_message_for_user_in_domain()
			throws DomainNotExistsException, UserNotExistsException,
			MessageNotFoundException {

		Domain domain = setDataDomain();

		service.addMessageInDomain(domain);
		Domain filter = new Domain.Builder().idGenerado(domain.getIdGenerado())
				.build();
		filter.addUserToDomain(new User.Builder().idGenerado(
				domain.getFirstUser().getIdGenerado()).build());
		assertThat(service.getAllMessages(filter).get(0).getFirstUser()
				.getListMessages().size(), is(2));

	}

	@Test(expected = UserNotExistsException.class)
	public void put_new_message_for_user_not_exists_in_domain()
			throws DomainNotExistsException, UserNotExistsException {

		Message message = new Message.Build().idGenerado("4").build();
		User user = new User.Builder().idGenerado("5").build();
		user.addMessage(message);
		Domain domain = new Domain.Builder().idGenerado("1").build();
		domain.addUserToDomain(user);

		service.addMessageInDomain(domain);

	}

	@Test
	public void delete_message_for_user_in_domain()
			throws DomainNotExistsException, UserNotExistsException {
		Domain domain = setDataDomain();
		service.removeMessage(domain);
		List<Domain> allMessages = service.getAllMessages(domain);
		assertThat(allMessages.get(0).getFirstUser().getListMessages()
				.isEmpty(), is(true));
	}

	@Test(expected = UserNotExistsException.class)
	public void delete_message_for_user_no_exists_in_domain()
			throws DomainNotExistsException, UserNotExistsException {

		Message message = new Message.Build().idGenerado("4").build();
		User user = new User.Builder().idGenerado("5").build();
		user.addMessage(message);
		Domain domain = new Domain.Builder().idGenerado("1").build();
		domain.addUserToDomain(user);

		service.removeMessage(domain);

	}

	@Test(expected = DomainNotExistsException.class)
	public void delete_message_for_user_in_domain_no_exists()
			throws DomainNotExistsException, UserNotExistsException {

		Message message = new Message.Build().idGenerado("4").build();
		User user = new User.Builder().idGenerado("1").build();
		user.addMessage(message);
		Domain domain = new Domain.Builder().idGenerado("3").build();
		domain.addUserToDomain(user);

		service.removeMessage(domain);

	}

	private Domain setDataDomain() {
		Domain domain = new Domain.Builder().nomDomain("domain")
				.idGenerado("1").build();
		Message message = new Message.Build().content("adwasdasd").from("pepe")
				.subject("asdas").idGenerado("4")
				.to(Arrays.asList(new String[] { "calamar", "ddd" })).build();
		User user = new User.Builder().userName("calamar").idGenerado("1")
				.build();
		user.addMessage(message);
		domain.addUserToDomain(user);
		return domain;
	}
}
