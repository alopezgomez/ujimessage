package org.uji.message.services;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.Before;
import org.junit.Test;
import org.uji.message.dto.Domain;
import org.uji.message.dto.User;
import org.uji.message.exception.UserNotExistsException;
import org.uji.message.security.Token;
import org.uji.message.security.TypeSecurity;
import org.uji.message.services.impl.AuthenticationServiceImpl;
import org.uji.message.utils.SecurityUtils;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

public class TestAuthenticationService {

	private DBCollection collection;
	private AuthenticationService authenticationService = new AuthenticationServiceImpl();

	@Before
	public void setUp() throws UnknownHostException {
		MongoClient client = new MongoClient();
		DB db = client.getDB("ujiMessage");
		collection = db.getCollection("domain");
	}

	@Test
	public void user_can_login_in_domain() throws UserNotExistsException,
			IOException, NoSuchAlgorithmException {

		Domain domain = setDataDomain();
		BasicDBObject domainInsert = insertDomainTest(domain);
		Domain domainFilter = new Domain.Builder()
				.idGenerado(domain.getIdGenerado())
				.nomDomain(domain.getNomDomain()).build();
		domainFilter.addUserToDomain(new User.Builder().userName("pepe")
				.password("pass").build());
		authenticationService.existsUser(domainFilter);
		collection.remove(domainInsert);

	}

	@Test(expected = UserNotExistsException.class)
	public void user_can_not_login_in_domain() throws UserNotExistsException,
			NoSuchAlgorithmException {

		Domain domain = setDataDomain();
		authenticationService.existsUser(domain);
	}

	@Test
	public void is_user_admin() throws IOException, UserNotExistsException,
			NoSuchAlgorithmException {

		BasicDBObject domainInsert = insertDomainTest(setDataDomain());
		Domain domain = setDataDomain();
		assertTrue(authenticationService.isAdminUser(domain));

		collection.remove(domainInsert);
	}

	@Test
	public void is_not_user_admin() throws IOException, UserNotExistsException,
			NoSuchAlgorithmException {

		BasicDBObject domainInsert = insertDomainTest(setDataDomain());
		Domain domain = setDataDomain();
		domain.getUserAdmin().setUserName("sssss");
		assertFalse(authenticationService.isAdminUser(domain));

		collection.remove(domainInsert);
	}

	private Domain setDataDomain() throws NoSuchAlgorithmException {
		Domain domain = new Domain.Builder().nomDomain("asdas")
				.idGenerado("32").build();

		User user = new User.Builder().idGenerado("233").userName("pepe")
				.build();
		Token password = SecurityUtils.getSecurityHashed(TypeSecurity.PASSWORD,
				new String[] { "pepe", "pass" });
		user.setPassword(password.getToken());
		domain.setUserAdmin(user);
		domain.addUserToDomain(user);
		return domain;
	}

	private BasicDBObject insertDomainTest(Domain domain)
			throws JsonGenerationException, JsonMappingException, IOException {
		BasicDBObject domainInsert = (BasicDBObject) JSON
				.parse(domain.toJSON());
		collection.insert(domainInsert);
		return domainInsert;
	}

}
