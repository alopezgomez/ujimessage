package org.uji.message.services;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.net.UnknownHostException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.Before;
import org.junit.Test;
import org.uji.message.dto.Chat;
import org.uji.message.dto.Domain;
import org.uji.message.dto.User;
import org.uji.message.exception.ChatNotExistException;
import org.uji.message.exception.ChatNotInitException;
import org.uji.message.services.impl.ChatServiceImpl;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

public class TestChatService {

	private static final String COLLECTION_DOMAIN = "domain";
	private static final String COLLECTION_CHAT = "chat";
	private ChatService chatService;
	private DBCollection collection;
	private DB db;
	private BasicDBObject objectDB;

	@Before
	public void setUp() throws UnknownHostException {
		chatService = new ChatServiceImpl();
		MongoClient client = new MongoClient();
		db = client.getDB("ujiMessage");
		collection = db.getCollection(COLLECTION_DOMAIN);
	}

	@Test
	public void is_possible_init_chat_beteween_users()
			throws ChatNotInitException, IOException {

		objectDB = prepareDataDomain();

		Chat chat = createValidChat();
		chatService.initChat(chat);

		removeData(chat);
	}

	@Test
	public void exists_chat_for_user() throws ChatNotExistException,
			IOException {
		objectDB = prepareDataDomain();

		Chat chat = createValidChat();
		collection = db.getCollection(COLLECTION_CHAT);
		BasicDBObject basicDBObject = (BasicDBObject) JSON.parse(chat.toJSON());
		collection.insert(basicDBObject);

		assertNotNull(chatService.getChat(chat));
		removeData(chat);

	}

	@Test(expected = ChatNotExistException.class)
	public void no_exists_chat_for_user() throws ChatNotExistException {
		Chat chat = createValidChat();

		assertNull(chatService.getChat(chat));

	}

	private void removeData(Chat chat) throws JsonGenerationException,
			JsonMappingException, IOException {

		if (collection.getName().equals(COLLECTION_CHAT)) {
			collection = db.getCollection(COLLECTION_DOMAIN);
		}
		collection.remove(objectDB);
		collection = db.getCollection(COLLECTION_CHAT);
		objectDB = (BasicDBObject) JSON.parse(chat.toJSON());
		collection.remove(objectDB);
	}

	private BasicDBObject prepareDataDomain() throws JsonGenerationException,
			JsonMappingException, IOException {
		Domain domain = new Domain.Builder().nomDomain(COLLECTION_DOMAIN)
				.build();
		domain.addUserToDomain(new User.Builder().userName("pepe").build());
		domain.addUserToDomain(new User.Builder().userName("dest").build());
		BasicDBObject dbObject = (BasicDBObject) JSON.parse(domain.toJSON());

		collection.insert(dbObject);
		return dbObject;
	}

	private Chat createInvalidChat() {
		Chat chat = new Chat();
		chat.setDomainName("98798");
		chat.setUserName("pepe");
		chat.setUserNameDest("dest");
		return chat;
	}

	private Chat createValidChat() {
		Chat chat = new Chat();
		chat.setDomainName(COLLECTION_DOMAIN);
		chat.setUserName("pepe");
		chat.setUserNameDest("dest");
		return chat;
	}
}