package org.uji.message.services;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.uji.message.dto.Domain;
import org.uji.message.dto.User;
import org.uji.message.exception.ContacNotExistsException;
import org.uji.message.exception.DomainNotExistsException;
import org.uji.message.services.impl.ContactServiceImpl;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

public class TestContactService {

	private DBCollection collection;
	private DBObject dbObject;
	private ContactService contact;

	@Before
	public void setUp() throws IOException {
		MongoClient client = new MongoClient();
		DB db = client.getDB("ujiMessage");
		collection = db.getCollection("domain");
		contact = new ContactServiceImpl();
		insertData();
	}

	@After
	public void tearDown() {
		collection.remove(dbObject);
	}

	@Test
	public void get_all_contact_for_domain() throws IOException,
			ContacNotExistsException, DomainNotExistsException {
		Domain domainFilter = new Domain.Builder().idGenerado("1").build();
		List<User> listResult = contact.getAllContacts(domainFilter);
		assertThat(listResult.size(), is(2));

	}

	@Test(expected = DomainNotExistsException.class)
	public void get_all_contact_for_no_exists_domain() throws IOException,
			ContacNotExistsException, DomainNotExistsException {
		Domain domainFilter = new Domain.Builder().idGenerado("3").build();
		contact.getAllContacts(domainFilter);
	}

	@Test
	public void get_contact_in_domain() throws ContacNotExistsException,
			DomainNotExistsException {

		Domain domainFilter = new Domain.Builder().idGenerado("1").build();
		domainFilter
				.addUserToDomain(new User.Builder().idGenerado("1").build());
		User user = contact.getContact(domainFilter);
		assertEquals("pepe", user.getUserName());

	}

	@Test(expected = DomainNotExistsException.class)
	public void get_contact_in_not_exists_domain()
			throws ContacNotExistsException, DomainNotExistsException {

		Domain domainFilter = new Domain.Builder().idGenerado("4").build();
		domainFilter
				.addUserToDomain(new User.Builder().idGenerado("1").build());
		User user = contact.getContact(domainFilter);
		assertEquals("pepe", user.getUserName());

	}

	@Test(expected = ContacNotExistsException.class)
	public void get_contact_no_exists_in_domain()
			throws ContacNotExistsException, DomainNotExistsException {

		Domain domainFilter = new Domain.Builder().idGenerado("1").build();
		domainFilter
				.addUserToDomain(new User.Builder().idGenerado("8").build());
		User user = contact.getContact(domainFilter);
		assertEquals("pepe", user.getUserName());

	}

	public void insertData() throws JsonGenerationException,
			JsonMappingException, IOException {
		Domain domain = new Domain.Builder().idGenerado("1").build();
		domain.addUserToDomain(new User.Builder().idGenerado("1")
				.userName("pepe").build());
		domain.addUserToDomain(new User.Builder().idGenerado("3")
				.userName("mama").build());
		dbObject = (DBObject) JSON.parse(domain.toJSON());
		collection.insert(dbObject);
	}

}
