package org.uji.message.services;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.net.UnknownHostException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.uji.message.dto.Domain;
import org.uji.message.dto.User;
import org.uji.message.exception.DomainAlreadyRegisteredException;
import org.uji.message.exception.DomainNotExistsException;
import org.uji.message.exception.UserExistsInDomainException;
import org.uji.message.exception.UserNotExistsException;
import org.uji.message.services.impl.AdminServiceImpl;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

public class TestAdminService {

	private AdminService service;
	private DBCollection collection;

	@Before
	public void setUp() throws UnknownHostException {
		service = new AdminServiceImpl();
		MongoClient client = new MongoClient();
		DB db = client.getDB("ujiMessage");
		collection = db.getCollection("domain");
	}

	@After
	public void tearDown() throws IOException {
		Domain domainDelete = new Domain.Builder().nomDomain("domain").build();
		DBCursor find = collection.find((DBObject) JSON.parse(domainDelete
				.toJSON()));
		while (find.hasNext()) {
			collection.remove(find.next());
		}
	}

	@Test
	public void user_added_to_domain() throws UserExistsInDomainException,
			IOException {

		prepareData();
		Domain domainAddUser = new Domain.Builder().idGenerado("1")
				.nomDomain("domain").build();
		addUsertoDomain(domainAddUser);
		service.addUsertToDomain(domainAddUser);

	}

	@Test(expected = UserExistsInDomainException.class)
	public void user_can_not_added_to_domain_because_exists()
			throws UserExistsInDomainException, IOException {

		Domain domain = prepareData();
		service.addUsertToDomain(domain);
		service.addUsertToDomain(domain);

	}

	@Test(expected = DomainNotExistsException.class)
	public void throw_exception_when_delete_domain_that_not_exists()
			throws DomainAlreadyRegisteredException, DomainNotExistsException {
		service.deleteDomain(new Domain());
	}

	@Test
	public void delete_domain() throws DomainAlreadyRegisteredException,
			DomainNotExistsException, IOException {

		Domain domain = prepareData();
		service.deleteDomain(domain);
	}

	@Test
	public void delete_user_in_domain() throws IOException,
			DomainNotExistsException, UserNotExistsException {
		Domain domain = prepareData();
		service.deleteUserInDomain(domain);
	}

	@Test(expected = DomainNotExistsException.class)
	public void try_to_delete_user_in_domain_not_exists()
			throws DomainNotExistsException, UserNotExistsException {
		Domain domain = createDomain();
		service.deleteUserInDomain(domain);
	}

	@Test(expected = UserNotExistsException.class)
	public void try_to_delete_user_not_exists_in_domain() throws IOException,
			DomainNotExistsException, UserNotExistsException {

		Domain domain = prepareData();
		Domain domainUserNoExists = new Domain.Builder()
				.nomDomain(domain.getNomDomain())
				.idGenerado(domain.getIdGenerado()).build();
		addUserInvalidtoDomain(domainUserNoExists);
		service.deleteUserInDomain(domainUserNoExists);
	}

	@Test
	public void get_all_user_for_domain() throws IOException,
			DomainNotExistsException {
		Domain domain = createDomain();
		addUsertoDomain(domain);
		insertDomain(domain);
		Domain allUsersInDomain = service.getAllUsersInDomain(domain);
		assertThat(allUsersInDomain.getListUsersDomain().size(), is(2));

	}

	@Test(expected = DomainNotExistsException.class)
	public void get_all_users_domain_not_exists_cause_exception()
			throws IOException, DomainNotExistsException {
		Domain domain = createDomain();
		addUsertoDomain(domain);
		service.getAllUsersInDomain(domain);

	}

	private Domain prepareData() throws IOException {
		Domain domain = createDomain();
		insertDomain(domain);
		return domain;
	}

	private Domain createDomain() {
		Domain domain = new Domain.Builder().idGenerado("1")
				.nomDomain("domain").build();
		domain.addUserToDomain(new User.Builder().userName("pepe")
				.password("asdas").build());
		return domain;
	}

	private void addUsertoDomain(Domain domain) {
		domain.addUserToDomain(new User.Builder().idGenerado("3")
				.userName("jasjda").password("dasdasd").build());
	}

	private void addUserInvalidtoDomain(Domain domain) {
		domain.addUserToDomain(new User.Builder().idGenerado("5")
				.userName("uyyuyu").password("dasdasd").build());
	}

	private void insertDomain(Domain domain) throws IOException {
		BasicDBObject domainInsert = (BasicDBObject) JSON
				.parse(domain.toJSON());
		collection.insert(domainInsert);
	}

}
