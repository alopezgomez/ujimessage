package org.uji.message.services;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestAdminService.class, TestAuthenticationService.class,
		TestChatService.class, TestServiceRegister.class,
		TestMessageService.class, TestContactService.class })
public class TestServices {

}
