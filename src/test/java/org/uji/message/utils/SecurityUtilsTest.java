package org.uji.message.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.security.NoSuchAlgorithmException;
import java.util.Date;

import org.junit.Test;
import org.uji.message.security.Token;
import org.uji.message.security.TypeSecurity;

public class SecurityUtilsTest {

	private Long getTimeMilis() {
		Long date = new Date().getTime();
		return date;
	}

	@Test
	public void generated_passwor_is_not_null() throws NoSuchAlgorithmException {

		String[] parms = { "user", "pass" };
		Token securityHashed = SecurityUtils.getSecurityHashed(
				TypeSecurity.PASSWORD, parms);
		assertNotNull("Password not generated", securityHashed);
	}

	@Test
	public void generated_token_is_not_null() throws NoSuchAlgorithmException {
		Long date = getTimeMilis();
		String[] parms = { "domain", "user", String.valueOf(date) };
		Token securityHashed = SecurityUtils.getSecurityHashed(
				TypeSecurity.SESSION_TOKEN, parms);
		assertNotNull("Session Token not generated", securityHashed);

	}

	@Test
	public void token_is_not_the_same_token_generated()
			throws NoSuchAlgorithmException {
		String[] parms = { "domain", "user", String.valueOf(getTimeMilis()) };
		Token securityHashedGenerated = SecurityUtils.getSecurityHashed(
				TypeSecurity.SESSION_TOKEN, parms);

		parms = new String[] { "domain", "user",
				String.valueOf(getTimeMilis() + 86) };
		Token securityHashed = SecurityUtils.getSecurityHashed(
				TypeSecurity.SESSION_TOKEN, parms);
		assertNotEquals(securityHashed, securityHashedGenerated);

	}

	@Test
	public void tokens_have_the_same_value() throws NoSuchAlgorithmException {

		String milis = String.valueOf(getTimeMilis());
		String[] parms = { "domain", "user", "192.168.2.1", milis };
		Token securityHashedGenerated = SecurityUtils.getSecurityHashed(
				TypeSecurity.SESSION_TOKEN, parms);
		Token securityHashed = SecurityUtils.getSecurityHashed(
				TypeSecurity.SESSION_TOKEN, parms);
		assertEquals(securityHashed, securityHashedGenerated);

	}
}
