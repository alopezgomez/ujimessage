package org.uji.message.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.uji.message.dto.TestDTO;

public class TestQueryUtils {

	@Test
	public void querySimple_expected_in_annotation_is_returned() {

		String queryExpected = "{cadenaTest:'aaa'}";

		String queryReturned = QueryUtils.getJSONQuery("querySimple",
				TestDTO.class, null);

		assertEquals(queryExpected, queryReturned);
	}

	@Test
	public void query_no_exists_in_annotation() {

		String queryReturned = QueryUtils.getJSONQuery("queryjjSimple",
				TestDTO.class, null);

		assertNull(queryReturned);
	}

	@Test
	public void queryNoSimple_expected_in_annotation_is_returned() {

		String queryExpected = "{cadenaTest:'aaa',listUsers.userName:'pepe'}";

		String queryReturned = QueryUtils.getJSONQuery("queryNoSimple",
				TestDTO.class, null);

		assertEquals(queryExpected, queryReturned);
	}

	@Test
	public void queryParameter_expected_in_annotation_is_returned() {

		String queryExpected = "{cadenaTest:'pepep',intTest:2}";

		String queryReturned = QueryUtils.getJSONQuery("queryParameter",
				TestDTO.class, new Object[] { "pepep", 2 });

		assertEquals(queryExpected, queryReturned);

	}

}
