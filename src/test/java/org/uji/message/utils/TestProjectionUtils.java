package org.uji.message.utils;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.Arrays;

import org.junit.Test;
import org.uji.message.dto.Domain;
import org.uji.message.dto.Message;
import org.uji.message.dto.User;

import com.mongodb.BasicDBObject;

public class TestProjectionUtils {

	@Test
	public void projection_is_not_null_for_simple_example() {

		Domain domain = new Domain.Builder().nomDomain("asdas").idGenerado("1")
				.build();
		try {
			BasicDBObject makeProjectionForMongoDTOObject = ProjectionUtils
					.projection(domain);
			assertNotNull(makeProjectionForMongoDTOObject.get("nomDomain"));
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	@Test
	public void projection_is_not_null_for_domain_with_user_admin() {
		Domain domain = new Domain.Builder().nomDomain("asdas").idGenerado("1")
				.userAdmin(createUser()).build();

		try {
			BasicDBObject makeProjectionForMongoDTOObject = ProjectionUtils
					.projection(domain);
			assertNotNull(makeProjectionForMongoDTOObject.get("userAdmin"));
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	@Test
	public void projection_is_not_null_for_domain_with_user_admin_and_listUsers() {
		Domain domain = new Domain.Builder().nomDomain("asdas").idGenerado("1")
				.userAdmin(createUser()).build();
		domain.addUserToDomain(createUser());

		try {
			BasicDBObject makeProjectionForMongoDTOObject = ProjectionUtils
					.projection(domain);
			assertNotNull(makeProjectionForMongoDTOObject.get("userAdmin"));
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	@Test
	public void projection_is_not_null_for_domain_with_user_admin_and_listUsers_and_messages_for_users() {
		Domain domain = new Domain.Builder().nomDomain("asdas").idGenerado("1")
				.userAdmin(createUser()).build();
		User user = createUser();
		user.addMessage(new Message.Build().from("pepe")
				.to(Arrays.asList(new String[] { "asda" }))
				.cco(Arrays.asList(new String[] { "asda" })).content("asds")
				.idGenerado("a").build());
		domain.addUserToDomain(user);

		try {
			BasicDBObject projection = ProjectionUtils.projection(domain);

			assertNotNull(projection
					.get("listUsersDomain.listMessages.idGenerado"));
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	private User createUser() {
		return new User.Builder().userName("asdasd").password("asdsad").build();
	}

}
