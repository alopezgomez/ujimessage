package org.uji.message.controller;

import org.junit.Rule;
import org.junit.runner.RunWith;

import com.eclipsesource.restfuse.Assert;
import com.eclipsesource.restfuse.Destination;
import com.eclipsesource.restfuse.HttpJUnitRunner;
import com.eclipsesource.restfuse.MediaType;
import com.eclipsesource.restfuse.Method;
import com.eclipsesource.restfuse.Response;
import com.eclipsesource.restfuse.annotation.Context;
import com.eclipsesource.restfuse.annotation.HttpTest;

@RunWith(HttpJUnitRunner.class)
public class TestManageRegister {

	@Rule
	public Destination destination = new Destination(this,
			"http://localhost:8080/message-api/api");

	@Context
	private Response response;

	@HttpTest(method = Method.PUT, path = "/register/domain", type = MediaType.APPLICATION_JSON, content = "{\"domainName\":\"prueba\",\"userName\":\"pepo\",\"password\":\"1234\"}")
	public void post_to_domain_assert_ok() {

		Assert.assertOk(response);
	}

}
