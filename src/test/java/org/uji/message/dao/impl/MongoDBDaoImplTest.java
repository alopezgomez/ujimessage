package org.uji.message.dao.impl;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.uji.message.dto.Message;
import org.uji.message.dto.TestDTO;
import org.uji.message.dto.User;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

public class MongoDBDaoImplTest {

	TestDTO test;

	MongoDBDaoImpl<String, TestDTO> mongoImpl;

	private DBCollection collection;

	@Before
	public void setUp() throws IOException {
		test = new TestDTO();
		mongoImpl = new MongoDBDaoImpl<String, TestDTO>(test);
		MongoClient client = new MongoClient();
		DB db = client.getDB("ujiMessage");
		collection = db.getCollection("test");
	}

	@After
	public void tearDown() {
		DBCursor find = collection.find();
		while (find.hasNext()) {
			collection.remove(find.next());
		}
	}

	@Test
	public void test_obtain_all_collection_data() {
		test.setCadenaTest("hola");
		test.setIntTest(3);

		try {
			String json = test.toJSON();
			BasicDBObject parse = (BasicDBObject) JSON.parse(json);
			collection.insert(parse);
		} catch (UnknownHostException e) {
			fail("fallo al conectar");
		} catch (IOException e) {
			fail("fallo al serializar");
		}
		assertThat(mongoImpl.find().size(), is(1));
	}

	@Test
	public void test_obtain_zero_elements_in_collection() {
		assertThat(mongoImpl.find().size(), is(0));
	}

	@Test
	public void test_insert_element_in_collection() {
		test = new TestDTO();
		test.setIdGenerado("3");
		User user = new User.Builder().userName("asdas").idGenerado("4")
				.password("asdas").build();
		Message message = new Message.Build().content("asddasas").from("pepe")
				.subject("asdasd")
				.to(Arrays.asList(new String[] { "asdas", "fdsdf" }))
				.cc(Arrays.asList(new String[] { "sdaa", "adsad" })).build();
		List<User> listUser = new ArrayList<>();
		user.addMessage(message);
		listUser.add(user);
		test.setListUsers(listUser);

		assertNotNull(mongoImpl.insert(test));
	}

	@Test
	public void test_delete_element_in_collection() throws IOException {
		String prepareData = prepareData();
		mongoImpl.delete(prepareData);
		assertNull(collection.findOne());
	}

	@Test
	public void test_obtain_element_by_query() throws IOException {
		prepareData();

		assertNotNull(mongoImpl.findOneByQuery(test.toJSON()));
	}

	@Test
	public void test_obtain_data_with_json_query() {
		prepareData();
		Object[] parms = new Object[] { "papapp", 2 };
		List<TestDTO> list = (List<TestDTO>) mongoImpl.findJSONQuery(
				"queryParameter", parms);
		assertThat(list.size(), is(1));

	}

	private String prepareData() {
		test = new TestDTO();
		test.setCadenaTest("papapp");
		test.setIntTest(2);
		return (String) mongoImpl.insert(test);
	}

}
